package anon.trollegle;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import static anon.trollegle.Util.t;

public class MultiUser extends UserConnection {
    
    public static final Comparator<MultiUser> compareId = new Comparator<MultiUser>() {
        @Override
        public int compare(MultiUser a, MultiUser b) {
            return a.number - b.number;
        }
    };

    public static final String MOTHERSHIP = "https://bellawhiskey.ca/trollegle/";
    public static final String LOGBOT_MOTHERSHIP = "https://api.pixelrain.info/logbot_auth";
    public static final String LOGBOT_MOTHERSHIP_TOR = "http://x7y3mat7k5eyortm.onion/logbot_auth";
    @Persistent
    private static boolean mothershipDown;
    @Persistent
    private static long warnTime = 5 * 60 * 1000;
    @Persistent
    private static long lurkTime = 22 * 60 * 1000;
    @Persistent
    private static long lurkInformTime = 4 * 60 * 1000;
    @Persistent
    private static double chatDab = Math.random();
    public enum LurkState { NORMAL, PRELURKER, LURKER }
    
    private Callback<? super MultiUser> callback;
    
    @Persistent
    private int number;
    @Persistent
    private String nick;
    @Persistent
    private boolean accepted, pulse, pulseEver, muted, uVerbose, flash, informed, 
        showNumbers, bot, lurkerFamiliarized;
    @Persistent
    private LurkState lastLurkState = LurkState.NORMAL;
    @Persistent
    private Boolean lurkPreference;
    private static final Set<String> nicksUsed = new HashSet<>();
    public final FloodMeter floodKickMeter = new FloodMeter(),
        floodControlMeter = new FloodMeter();
    @Persistent
    private long connectTime;
    @Persistent
    private long lastWarning;
    @Persistent
    private long lastMessage;
    @Persistent
    private int lurkCount;
    @Persistent
    private String pulseWord, pulseName;
    private Object adminToken;
    @Persistent
    private String logBotSalt = "";
    @Persistent
    private String kickReason;
    
    @Persistent
    int patCount;
    @Persistent
    long lastPat;
    
    public MultiUser(Callback<? super MultiUser> callback) {
        super(null);
        this.callback = callback;
        
        synchronized (nicksUsed) {
            do {
                nick = Util.chooseName();
            } while (nicksUsed.contains(nick));
            nicksUsed.add(nick);
        }
        
        generatePulse();
    }
    
    public static long getLurkTime() {
        return lurkTime;
    }
    public static void setLurkTime(long lurkTime) {
        MultiUser.lurkTime = lurkTime;
    }
    
    public void recordMessage() {
        if (!isLurker())
            lastMessage = System.currentTimeMillis();
    }
    
    public LurkState lurkState() {
        Boolean lurkPreferenceAtomic = lurkPreference;
        if (lurkPreferenceAtomic != null)
            return lurkPreferenceAtomic ? LurkState.LURKER : LurkState.NORMAL;
        if (lurkTime < 1 || lastMessage == 0)
            return LurkState.NORMAL;
        if (System.currentTimeMillis() - lastMessage > lurkTime + lurkInformTime)
            setLurkPreference(true);
        if (System.currentTimeMillis() - lastMessage > lurkTime)
            return LurkState.LURKER;
        if (System.currentTimeMillis() - lastMessage > lurkTime - lurkInformTime)
            return LurkState.PRELURKER;
        return LurkState.NORMAL;
    }
    
    public boolean isLurker() {
        return lurkState() == LurkState.LURKER;
    }

    public void warn() {
        lastWarning = System.currentTimeMillis();
    }

    public boolean isWarned() {
        return System.currentTimeMillis() - lastWarning < warnTime;
    }

    public int lurk() {
        return ++lurkCount;
    }

    public String getNick() { return nick; }
    
    @Override
    protected void callback(String action, String data) {
        callback.callback(this, action, data);
    }
    @Override
    protected void callback(String action, int data) {
        callback.callback(this, action, data);
    }
    
    @Override
    public void run() {
        try {
            super.run();
        } finally {
            synchronized (nicksUsed) {
                nicksUsed.remove(nick);
            }
            callback("disconnected", null);
        }
    }
    
    @Override
    public String getDisplayNick() { return nick; }
    
    @Override
    public MultiUser fill(boolean accepted, boolean questionMode, ProxyConfig proxy) {
        this.accepted = accepted;
        super.fill(questionMode, proxy);
        return this;
    }
    
    @Override
    public MultiUser withLang(String lang) {
        super.withLang(lang); return this;
    }
    @Override
    public MultiUser withTopics(String... topics) {
        super.withTopics(topics); return this;
    }
    
    @Override
    protected void banCleanup() {
        if (pulse) {
            sendPulse(false);
            pulse = false;
        }
    }
    
    @Override
    protected void startSent() {
        super.startSent();
        if (pulse) {
            sendPulse(true);
        }
    }
    
    @Override
    protected void connected() {
        if (isConnected())
            return;
        if (pulse) {
            sendPulse(false);
            pulse = false;
        }
        lastMessage = System.currentTimeMillis();
        super.connected();
        if (!accepted && isQuestionMode())
            log("[] " + getDisplayNick() + " has connected");
    }
    
    public void accept() {
        setAccepted(true);
        setBorn();
        setMsgCount(0);
    }
    
    @Override
    protected void setBorn() {
        if (accepted)
            super.setBorn();
        else
            connectTime = System.currentTimeMillis();
    }
    
    @Override
    protected void hasCommonLikes(String commonLikes) {
        if (isPulseEver())
            callback("pulse success", commonLikes);
        else
            callback("interests", commonLikes);
    }
    
    @Override
    public void dispose() {
        if (pulse) {
            sendPulse(false);
            pulse = false;
        }
        super.dispose();
    }
    
    @Override
    public MultiUser dummy() {
        super.dummy();
        accepted = true;
        setNick("Admin");
        setLurkPreference(false);
        return this;
    }

    @Override
    public String getTrivia() {
        if (isPulseEver())
            return "(pulse) " + super.getTrivia();
        return super.getTrivia();
    }

    public void setNick(String nick) {
        synchronized (nicksUsed) {
            if (nicksUsed.contains(nick)) {
                throw new IllegalArgumentException("nick already used");
            }
            nicksUsed.remove(this.nick);
            this.nick = nick;
            nicksUsed.add(nick);
        }
    }

    public Object getAdminToken() { return adminToken; }
    public void setAdminToken(Object adminToken) { this.adminToken = adminToken; }
    
    public boolean isFlash() { return flash; }
    public void setFlash(boolean flash) { this.flash = flash; }

    public int getNumber() { return number; }
    public MultiUser withNumber(int number) {
        if (this.number != 0)
            throw new RuntimeException("Can't give a number to someone who already has one!");
        this.number = number;
        return this;
    }
    
    public boolean isAccepted() { return accepted; }
    protected void setAccepted(boolean accepted) { this.accepted = accepted; }
    
    public void setVerbose(boolean verbose) { this.uVerbose = verbose; }
    public boolean isVerbose() { return uVerbose; }

    public void setMuted(boolean muted) { this.muted = muted; } 
    public boolean isMuted() { return muted; }

    public void showNumbers(boolean showNumbers) { this.showNumbers = showNumbers; } 
    public boolean showsNumbers() { return showNumbers; }

    public void setBot(boolean bot) { this.bot = bot; } 
    public boolean isBot() { return bot; }

    public void setLurkPreference(Boolean lurkPreference) {
        if (lurkPreference == Boolean.FALSE)
            lastMessage = System.currentTimeMillis();
        this.lurkPreference = lurkPreference;
    } 
    public Boolean getLurkPreference() { return lurkPreference; }
    
    public boolean shouldInformLurker() {
        LurkState previousState = lastLurkState;
        lastLurkState = lurkState();
        return previousState != lastLurkState;
    }
    public boolean shouldFamiliarizeLurker() {
        if (lurkerFamiliarized)
            return false;
        lurkerFamiliarized = true;
        return true;
    }
    
    public void setKickReason(String kickReason) { this.kickReason = kickReason; } 
    public String getKickReason() { return kickReason; }
    
    public void setLogBotSalt(String logBotSalt) { this.logBotSalt = logBotSalt; } 
    public String getLogBotSalt() { return logBotSalt; }
    
    public void inform() { informed = true; }
    public boolean isInformed() { return informed; }

    public boolean isPulse() { return pulse; }

    public boolean isPulseEver() { return pulseEver; }
    
    public void sendCaptcha(String response) {
        super.sendCaptcha(response);
        if (pulseEver) {
            pulse = true;
            sendPulse(true);
        }
    }

    @Override
    public boolean isReady() {
        return super.isReady() && accepted;
    }

    @Override
    public boolean didLive() {
        return accepted && super.didLive();
    }

    public String getPulseWords() {
        return pulseWord + ", " + pulseName;
    }
    
    @Override
    protected String getRelevantAge() {
        if (isConnected() && !accepted)
            return t("unconfirmedage", Util.minSec(System.currentTimeMillis() - connectTime));
        return super.getRelevantAge();
    }

    public MultiUser pulse() {
        if (mothershipDown && Math.random() > 0.66)
            withTopics(pulseWord, pulseName, "trollegle", "bellawhiskey");
        else 
            withTopics(pulseWord, pulseName);
        this.pulse = true;
        this.pulseEver = true;
        return this;
    }

    public void unpulse(boolean convert) {
        sendPulse(false);
        pulse = false;
        if (convert) {
            setAccepted(false);
            stopLookingForCommonLikes();
        }
    }

    protected void sendPulse(boolean on) {
        if (mothershipDown && !on) {
            log("Skipping pulse removal for " + getDisplayNick() + " (mothership down)");
            return;
        }
        try {
            URL homeUrl = new URL(MOTHERSHIP + pulseWord + "," + pulseName + "-" + (on ? "1" : "0") + "-" + chatDab);
            URLConnection conn = homeUrl.openConnection();
            conn.setDoOutput(true);
            Util.fillHeaders(conn, "json post");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write("");
            wr.flush();
            BufferedReader rd
                    = (new BufferedReader(new InputStreamReader(conn.getInputStream())));
            String line = rd.readLine();
            String[] parts = line.split(", name: ");
            if (parts.length > 1) {
                callback("chatname", parts[1]);
            }
            log("Pulse for " + getDisplayNick() + " (front " + server + "): " + line + ", interests: " + getPulseWords());
            mothershipDown = false;
            wr.close();
            rd.close();
        } catch (Exception ex) {
            log("Failed to send pulse for " + getDisplayNick());
            mothershipDown = true;
            if (verbose) {
                ex.printStackTrace();
            }
        }
    }
    
    protected Boolean authLogBot(String string, ProxyConfig proxy) throws IOException {
        if (proxy == null)
            proxy = getProxy();
        URL url = new URL(proxy.isTor() ? LOGBOT_MOTHERSHIP_TOR : LOGBOT_MOTHERSHIP);
        URLConnection conn = url.openConnection(proxy.getProxy());
        conn.setRequestProperty("pragma", "no-cache");
        conn.setRequestProperty("accept-language", "en-GB,en");
        conn.setRequestProperty("upgrade-insecure-requests", "1");
        conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");
        conn.setRequestProperty("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        conn.setRequestProperty("cache-control", "no-cache");
        conn.setRequestProperty("authority", "pixelrain.info");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = rd.readLine();
        rd.close();
        String expected = Util.sha256(string + "-" + System.currentTimeMillis() / 100000 +
                "-" + (proxy.isTor() ? "TOR" : proxy.updateAndGetExitAddress()));
        return line.equals(expected);
    }

    protected void generatePulse() {
        setPulse(Util.chooseName(), Util.chooseWord());
    }
    
    protected void setPulse(String name, String word) {
        if (pulseName != null || pulseWord != null)
            throw new RuntimeException("trying to set multiple pulses for user" + this);
        pulseName = name;
        pulseWord = word;
    }
    
    private static final JsonSerializer<MultiUser> serializer = new JsonSerializer<>(MultiUser.class);

    // FIXME: blocking sendPulse here is ultra iffy
    
    @Override
    public JsonValue killAndSave() {
        if (pulse && isCreated() && getCaptchaSiteKey() == null)
            sendPulse(false);
        return serializer.save(this, super.killAndSave());
    }
    
    @Override
    public MultiUser load(JsonValue spec) {
        super.load(spec);
        serializer.load(this, spec);
        nicksUsed.add(nick);
        if (pulse && isCreated() && getCaptchaSiteKey() == null)
            sendPulse(true);
        return this;
    }
    
    public static JsonValue saveStatic() {
        return serializer.save(null, UserConnection.saveStatic());
    }
    
    public static void loadStatic(JsonValue spec) {
        UserConnection.loadStatic(spec);
        serializer.load(null, spec);
    }

}
