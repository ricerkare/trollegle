package anon.trollegle;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    private static final Logger LOGGER = Logger.getLogger(Util.class.getName());

    private static String[] names = null;
    private static String[] words = null;
    private static Map<String, String> messages = null;
    private static Map<String, String> flashisms = null;
    
    static {
        try {
            String messageFile = System.getProperty("messageFile");
            messages = Util.loadMessagesFromResource(messageFile == null ? "/data/trollegle.messages" : messageFile);
            names = Util.loadStringsFromResource("/data/names");
            words = Util.loadStringsFromResource("/data/words");
            flashisms = Util.loadFlashismsFromResource("/data/flashisms");
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Failed to load names/words :v", ex);
        }
    }

    public static void forEachLineInResource(String resourcePath, Consumer<String> action) throws IOException {
        InputStream is;
        Scanner scanner;

        is = Util.class.getResource(resourcePath).openStream();
        scanner = new Scanner(is);

        while (scanner.hasNextLine())
            action.accept(scanner.nextLine());

        scanner.close();
        is.close();

    }
    
    public static String[] loadStringsFromResource(String resourcePath) throws IOException {
        ArrayList<String> read = new ArrayList<>();
        forEachLineInResource(resourcePath, line -> { if (!line.isEmpty()) read.add(line); });
        return read.toArray(new String[read.size()]);
    }
    
    public static Map<String, String> loadMessagesFromResource(String resourcePath) throws IOException {
        HashMap<String, String> read = new HashMap<>();
        forEachLineInResource(resourcePath, line -> {
            if (line.isEmpty()) return;
            String[] split = line.split(" ", 2);
            if (split.length == 2)
                read.put(split[0], split[1]);
            else
                LOGGER.log(Level.WARNING, "Badly formed message: " + line);
        });
        return Collections.unmodifiableMap(read);
    }
    
    static String wholeWord(String word) { return "(^|\\s+)" + word + "\\b"; }
    
    public static Map<String, String> loadFlashismsFromResource(String resourcePath) throws IOException {
        HashMap<String, String> read = new LinkedHashMap<>();
        forEachLineInResource(resourcePath, line -> {
            if (line.isEmpty()) return;
            String[] split = line.split("\t", 2);
            if (split.length == 2) {
                read.put(wholeWord(split[0].toUpperCase()), " " + split[1].toUpperCase());
                read.put(wholeWord(split[0]), " " + split[1]);
            } else {
                LOGGER.log(Level.WARNING, "Badly formed flashism: " + line);
            }
        });
        return Collections.unmodifiableMap(read);
    }
    
    public static String unflash(String message) {
        for (Map.Entry<String, String> entry : flashisms.entrySet()) {
            message = message.replaceAll(entry.getKey(), entry.getValue());
        }
        return message;
    }
    
    public static String tn(String msgid, Object... args) {
        String message = messages.get(msgid);
        if (message != null) {
            message = message.replace("\\n", "\n");
            if (args == null || args.length == 0)
                return message;
            else
                return String.format(message, args);
        } else {
            return null;
        }
    }
    
    public static String t(String msgid, Object... args) {
        String message = tn(msgid, args);
        if (message == null)
            return "Couldn't find message named '" + msgid + "' (called with: " + Arrays.toString(args) + ")";
        return message;
    }
    
    private static final Map<String, String[]> tClumpCache = new HashMap<>();
    
    public static String[] tnClump(String msgid) {
        String[] messages = tClumpCache.get(msgid);
        if (messages == null) {
            String clump = t(msgid);
            if (clump == null)
                return null;
            messages = clump.split("\\|");
            tClumpCache.put(msgid, messages);
        }
        return messages;
    }
    
    public static String[] tClump(String msgid) {
        String[] messages = tnClump(msgid);
        if (messages == null)
            return new String[] { msgid };
        return messages;
    }
    
    public static String tnRandom(String msgid, Object... args) {
        String[] messages = tnClump(msgid);
        if (messages == null)
            return null;
        if (args == null || args.length == 0)
            return randomize(messages);
        return String.format(randomize(messages), args);
    }
    
    public static String tRandom(String msgid, Object... args) {
        String message = tnRandom(msgid, args);
        if (message == null)
            return "Couldn't find message named '" + msgid + "' (called with: " + Arrays.toString(args) + ")";
        return message;
    }
    
    public static String tRandomConcat(String... msgids) {
        StringBuilder b = new StringBuilder();
        for (String msgid : msgids) {
            b.append(tRandom(msgid));
        }
        return b.toString();
    }
    
    public static String tRandomLine(String parentMsgid) {
        return tRandomConcat(tClump(parentMsgid));
    }
    
    private static final String[] spaces = {" ", " ", " ", "  ", "  ", "   "};
    public static String spaceball(String quoted) {
        Pattern pat = Pattern.compile("  ?");
        Matcher mat = pat.matcher(quoted);
        StringBuffer result = new StringBuffer();
        while (mat.find()) {
            mat.appendReplacement(result, randomize(spaces));
        }
        mat.appendTail(result);
        if (result.length() > 0 && result.charAt(result.length() - 1) != ' '
                && Math.random() > 0.5)
            result.append(randomize(spaces));
        quoted = result.toString();
        return quoted;
    }
    
    /** DateFormats are for enterprises~~ */
    public static String minSec(long time) {
        long sec = time / 1000;
        long min = sec / 60;
        if (Math.abs(min) > 90) {
            return min / 60 + " h " + Math.abs(min) % 60 + " min";
        } else if (min != 0) {
            return min + " min " + Math.abs(sec) % 60 + " s";
        }
        long ms = time % 1000;
        if (sec < 20 && ms != 0) {
            int tail = 0;
            for (; time % 10 == 0; tail++)
                time /= 10;
            String result = sec == 0 && ms < 0
                ? String.format("-0.%03d", Math.abs(ms))
                : String.format("%d.%03d", sec, Math.abs(ms));
            return result.substring(0, result.length() - tail) + " s";
        }
        return sec + " s";
    }
    
    public static String minSec(long time, String returnIfNegative) {
        if (time < 0)
            return returnIfNegative;
        return minSec(time);
    }
    
    public static long parseTime(String text) {
        return TimeParser.parse(text);
    }
    
    /** InterruptedException? What's that?! */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static final String RANDID_PIECES = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
    public static final String ALNUM_PIECES = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    
    public static String makeRandid() {
        return makeRandid(RANDID_PIECES, 8);
    }

    public static String makeRandid(String pieces, int length) {
        StringBuilder b = new StringBuilder();
        while (b.length() < length) {
            b.append(pieces.charAt((int) (pieces.length() * Math.random())));
        }
        return b.toString();
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T randomize(T... choices) {
        return choices[(int) (Math.random() * choices.length)];
    }

    public static String randomize(String[]... choices) {
        StringBuilder b = new StringBuilder();
        for (String[] set : choices) {
            b.append(randomize(set));
        }
        return b.toString();
    }
    
    public static String chooseName() {
        return randomize(names);
    }
    
    public static String chooseWord() {
        return randomize(words);
    }
    
    public static String sha1(String message) {
        return md("SHA-1", message);
    }
    public static String sha256(String message) {
        return md("SHA-256", message);
    }
    private static char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    public static String md(String function, String message) {
        try {
            MessageDigest digest = MessageDigest.getInstance(function);
            byte[] hash = digest.digest(message.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (byte b : hash)
                sb.append(digits[b >>> 4 & 15]).append(digits[b & 15]);
            return sb.toString();
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw (RuntimeException) e;
            throw new RuntimeException(e);
        }
    }
    
    public static CharSequence reprString(String in) {
        StringBuilder out = new StringBuilder("\"");
        for (int i = 0, length = in.length(); i < length; i++) {
            char c = in.charAt(i);
            if (c == '\b') out.append("\\b");
            else if (c == '\f') out.append("\\f");
            else if (c == '\n') out.append("\\n");
            else if (c == '\r') out.append("\\r");
            else if (c == '\t') out.append("\\t");
            else if (c == '\"') out.append("\\\"");
            else if (c == '\\') out.append("\\\\");
            else if (c < 0x7f) out.append(c);
            else out.append(String.format("\\u%04x", (int) c));
        }
        out.append("\"");
        return out;
    }
    
    public static void fillUserAgent(URLConnection conn) {
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:50.0) Gecko/20100101 Firefox/50.0");
    }

    public static void fillHeaders(URLConnection conn, String s) {
        conn.setConnectTimeout(2 * 60 * 1000);
        conn.setReadTimeout(2 * 60 * 1000);
        s = " " + s + " ";
        fillUserAgent(conn);
        if (s.contains(" json ")) {
            conn.setRequestProperty("Accept", "application/json");
        } else {
            conn.setRequestProperty("Accept", "text/javascript, text/html, application/xml, text/xml, */*");
        }
        if (s.contains(" post ")) {
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        }
        conn.setRequestProperty("Accept-Language", "en-US;en;q=0.5");
        conn.setRequestProperty("origin", "https://www.omegle.com");
        conn.setRequestProperty("Referer", "https://www.omegle.com/");
        /*conn.setRequestProperty("Cookie", "randid=" + randid);
         conn.setRequestProperty("Cookie", "__cfduid=dfae36274feb8aa94d2e83a69086255d91470913354");
         conn.setRequestProperty("Cookie", "fblikes=0");*/
    }
    
    public static String urlEncode(String in) {
        try {
            return URLEncoder.encode(in, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static boolean isTailEat(String data) {
        return data.matches(".* There +(is|are) .*[0-9].* in +the .*") || data.matches(".*[0-9].* (is|are) +in +the .*");
    }
    
    public static Map<String, Object> mapHack(Object... kv) {
        Map<String, Object> map = new LinkedHashMap<>();
        for (int i = 1; i < kv.length; i += 2)
            if (kv[i] != null)
                map.put((String) kv[i - 1], kv[i]);
        return map;
    }
    
    public static <T> void forEach(T[] array, Consumer<? super T> body) {
        for (T t : array)
            body.accept(t);
    }
    
    public static <T> boolean any(T[] array, Predicate<? super T> body) {
        for (T t : array) 
            if (body.test(t))
                return true;
        return false;
    }
    
    public static <T> boolean contains(T[] array, T element) {
        if (element == null) {
            for (T t : array)
                if (t == null)
                    return true;
        } else {
            for (T t : array)
                if (element.equals(t))
                    return true;
        }
        return false;
    }
    
    public static <T> boolean all(T[] array, Predicate<? super T> body) {
        for (T t : array) 
            if (!body.test(t))
                return false;
        return true;
    }
    
    public static <T, F> T[] map(F[] from, Function<? super F, ? extends T> body, IntFunction<T[]> constructor) {
        T[] to = constructor.apply(from.length);
        for (int i = 0; i < from.length; i++)
            to[i] = body.apply(from[i]);
        return to;
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T[] filter(T[] array, Predicate<? super T> body) {
        T[] result = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);
        int pos = 0;
        for (T t : array)
            if (body.test(t))
                result[pos++] = t;
        if (pos == array.length)
            return result;
        return Arrays.copyOf(result, pos);
    }
    
    public static <T> T[] concat(T[] one, T[] two) {
        T[] result = Arrays.copyOf(one, one.length + two.length);
        System.arraycopy(two, 0, result, one.length, two.length);
        return result;
    }
    
    @Deprecated
    public interface ThrowingRunnable {
        void run() throws Exception;
    }
    public interface PassingRunnable {
        void run(Exception exception) throws Exception;
    }
    
    @Deprecated
    public static void retry(int times, String description, ThrowingRunnable action) {
        for (; ; times--)
            try {
                action.run();
                break;
            } catch (Exception e) {
                if (times < 1)
                    throw e instanceof RuntimeException ? (RuntimeException) e : new RuntimeException(e);
                System.err.println(description + " (will retry " + times + "x): " + e);
                sleep(250);
            }
    }
    
    public static void retry(int times, String description, PassingRunnable action) {
        Exception lastException = null;
        for (; ; times--)
            try {
                action.run(lastException);
                break;
            } catch (Exception e) {
                if (times < 1)
                    throw e instanceof RuntimeException ? (RuntimeException) e : new RuntimeException(e);
                lastException = e;
                System.err.println(description + " (will retry " + times + "x): " + e);
                sleep(250);
            }
    }
    
    public static <T> Set<T> addMapping(Map<T, Set<T>> map, T outer, T inner) {
        Set<T> set = map.get(outer);
        synchronized (map) {
            if (set == null) {
                set = new HashSet<>();
                map.put(outer, set);
            }
            set.add(inner);
        }
        return set;
    }
    
    public static <T> void removeMapping(Map<T, Set<T>> map, T outer, T inner) {
        Set<T> set = map.get(outer);
        if (set == null)
            return;
        synchronized (map) {
            set.remove(inner);
            if (set.isEmpty())
                map.remove(outer);
        }
    }
    
    public static <T> void clearMappings(Map<T, Set<T>> map, T item) {
        synchronized (map) {
            map.remove(item);
            for (Set<T> subset : map.values())
                subset.remove(item);
            map.values().removeIf(Collection::isEmpty);
        }
    }

}
