package anon.trollegle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.AbstractCollection;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static anon.trollegle.Util.addMapping;
import static anon.trollegle.Util.clearMappings;
import static anon.trollegle.Util.randomize;
import static anon.trollegle.Util.removeMapping;
import static anon.trollegle.Util.t;
import static anon.trollegle.Util.tRandom;
import static anon.trollegle.Util.tRandomLine;

public class Multi implements Callback<MultiUser>, Proxies.Listener {

    List<MultiUser> users = new ArrayList<>();
    private MultiUser consoleDummy;
    private List<String> rcFiles = new ArrayList<>();
    private List<String> jsonFiles = new ArrayList<>();
    
    protected final Proxies proxies = makeProxies();
    protected Proxies makeProxies() { return new Proxies(this); }
    @Persistent
    protected boolean moveProxies = false;
    
    private AdminCommands commands = makeAdminCommands();
    private UserBehaviour userBehaviour = makeUserBehaviour();
    private BotBehaviour botBehaviour = makeBotBehaviour();
    protected AdminCommands makeAdminCommands() { return new AdminCommands(this); }
    protected UserBehaviour makeUserBehaviour() { return new UserBehaviour(this); }
    protected BotBehaviour makeBotBehaviour() { return new BotBehaviour(this); }
    
    Collection<MultiUser> allUsers = new AbstractCollection<MultiUser>() {
        public int size() {
            return consoleDummy == null ? users.size() : 1 + users.size();
        }
        public Iterator<MultiUser> iterator() {
            return new Iterator<MultiUser>() {
                Iterator<MultiUser> back =
                    consoleDummy == null ? users.iterator() : null;
                public MultiUser next() {
                    if (back != null)
                        return back.next();
                    back = users.iterator();
                    return consoleDummy;
                }
                public boolean hasNext() {
                    return back == null || back.hasNext();
                }
            };
        }
    };

    private Map<MultiUser, Set<MultiUser>> kickVotes = new WeakHashMap<>();
    private Map<MultiUser, Set<MultiUser>> ignored = new WeakHashMap<>();

    @Persistent
    private Ban[] bans = {};
    @Persistent
    private final Set<String> flashes = new LinkedHashSet<>();
    private final Object banLock = new Object();

    @Persistent
    long joinFreq = 3000, joinFreqConstant = 4000, pulseFreq = 700000;
    @Persistent
    int maxChatting = 9, maxTotal = 11;
    @Persistent
    long floodKickTime = 7500;
    @Persistent
    int floodKickSize = 4;
    @Persistent
    long floodControlTime = 3500;
    @Persistent
    int floodControlSpan = 9;
    @Persistent
    private long inactiveBot = 13 * 30 * 1000, 
        lurkingHuman = 3 * 60 * 1000, 
        inactiveHuman = 13 * 60 * 1000, 
        matureHuman = 90000,
        disablePulseTime, pulseCooldownTime,
        patCooldown = 5000;
    @Persistent
    double qfreq = 0.9, muteBotFreq = 0.9;
    @Persistent
    private boolean autoJoin;
    @Persistent
    private long lastInvite = System.currentTimeMillis();
    @Persistent
    private long lastPulse = System.currentTimeMillis();
    @Persistent
    private long lastPulseTailEat, pulseTailEatBackoff = 15 * 60 * 1000;
    @Persistent
    private double minIntervalExponent = 2d/3;
    @Persistent
    int banSpan = 60;
    @Persistent
    long banSpanTime = -1;
    @Persistent
    boolean murder;
    @Persistent
    boolean countMuted = true, showLurkChanges = false;
    @Persistent
    String challenge = Util.makeRandid() + Util.makeRandid();
    @Persistent
    String password = Util.makeRandid() + Util.makeRandid();
    Object adminToken = new Object();
    @Persistent
    private String chatName = "(none)";
    
    protected static class Message {
        MultiUser user;
        String text;
    }
    private Message[] history = {};
    private final Object historyLock = new Object();
    @Persistent
    private int historyPosition;
    
    enum JoinType { TEXT, QUESTION, PULSE };
    @Persistent
    EnumSet<JoinType> toothpasteTypes = EnumSet.of(JoinType.TEXT);
    
    private MultiUser[] ids = new MultiUser[maxTotal * 3 / 2];
    @Persistent
    private int lastFreedId = -1;
    
    private WeakReference<MultiUser> logBot = new WeakReference<>(null);
    @Persistent
    private boolean logBotEnabled, logBotPrivileged;
    private final Map<String, LogBotRequest> logBotRequests = new HashMap<>();
    @Persistent
    private long logBotTimeout = 15000;
    @Persistent
    boolean logBotAuthEnabled;
    
    @Persistent
    private Pattern toothpastePattern = Pattern.compile(t("toothpaste"));
    @Persistent
    private String toothpastePrompt = t("toothpasteprompt");
    
    public Multi() {
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    long loopFreq = Math.min(250, joinFreq * Math.max(spamSize() - 1, 1));
                    Util.sleep(loopFreq / 2 + (long) (Math.random() * loopFreq));
                    mainLoop(false);
                }
            }
        }.start();
    }
    
    // Following three shouldn't be accessed outside of mainLoop!
    private ArrayDeque<MultiUser> kickTargets = new ArrayDeque<>();
    private ArrayDeque<MultiUser> unpulseTargets = new ArrayDeque<>();
    private HashMap<ProxyConfig, int[]> zombies = new HashMap<>();
    @Persistent
    private boolean lastWasTriggered;
    protected synchronized void mainLoop(boolean triggered) {
        if (lastWasTriggered && triggered)
            return;
        lastWasTriggered = triggered;
        try {
            wipeLogBotRequests();
            
            if ((autoJoin || pulseFreq > -1) && !proxies.isBanned())
                proxies.switchIfStale();

            int pulses = 0;
            zombies.clear();
            final HashSet<ProxyConfig> startedProxies = new HashSet<>();
            final HashSet<ProxyConfig> usedProxies = new HashSet<>();

            synchronized (users) {
                for (MultiUser u : users) {
                    if (u.isPulse()) {
                        if (u.idleFor() > pulseFreq || getPulseCooldown() > 100) {
                            unpulseTargets.add(u);
                        } else {
                            pulses++;
                        }
                        continue;
                    }
                    if ((!u.isReady() || u.isMuted()) && u.idleFor() > inactiveBot
                            || u.idleFor() > inactiveHuman * (!shouldRevive() ? 3 : 1)) {
                        kickTargets.add(u);
                    } else if (u.shouldInformLurker()) {
                        tellRoom(to -> behave(to).updateLurk(to, u));
                    }
                    if (u.idleFor() > inactiveBot && !u.isConnected() && !u.isPulseEver()) {
                        int[] count = zombies.get(u.getProxy());
                        if (count == null) {
                            zombies.put(u.getProxy(), new int[] {1});
                        } else {
                            count[0]++;
                        }
                    } else if (u.isConnected()) {
                        startedProxies.add(u.getProxy());
                        if (u.isUsingProxy())
                            usedProxies.add(u.getProxy());
                    }
                }
            }
            while (!kickTargets.isEmpty())
                kickInactive(kickTargets.pop());
            
            while (!unpulseTargets.isEmpty())
                unpulseTargets.pop().unpulse(autoJoin 
                        && System.currentTimeMillis() - lastPulseTailEat > pulseTailEatBackoff
                        && Math.random() * (maxChatting() + 1) > spamSize());
            
            for (Map.Entry<ProxyConfig, int[]> entry : zombies.entrySet())
                if (entry.getValue()[0] > 1)
                    banned(entry.getKey(), "zombie users");

            if (autoJoin && System.currentTimeMillis() - lastInvite >= joinFreq() && !proxies.isBanned()
                    && spamSize() < maxChatting() && users.size() < maxTotal() && !proxies.isDirty()) {
                add();
                lastInvite = System.currentTimeMillis();
            }
            
            if (pulses == 0)
                notifyPulseResumed();
            if (pulses == 0 && shouldAddPulse()) {
                if (!proxies.isDirty() || proxies.switchProxy())
                    if (addPulse())
                        lastPulse = System.currentTimeMillis();
            }
            
            StringBuilder started = proxies.startTorSearches(usedProxies);
            proxies.pingStatus(startedProxies);
            
            if (started.length() > 0)
                tellAdmin("Starting Tor search for: " + started.substring(2));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    protected boolean shouldAddPulse() {
        return pulseFreq >= 0 
            && getPulseCooldown() == 0
            && System.currentTimeMillis() - lastPulse >= pulseFreq
            && !proxies.isBanned()
            && users.size() < maxTotal();
    }
    
    /** Only here for overriding. Use makeIdUser everywhere else. */
    protected MultiUser makeUser() {
        return new MultiUser(this);
    }
    
    protected MultiUser makeIdUser() {
        synchronized (users) {
            resizeIds();
            int id = freeId();
            ids[id] = makeUser().withNumber(id);
            ids[id].setMoveProxy(moveProxies);
            return ids[id];
        }
    }
    
    public void massacre() {
        murder = true;
        autoJoin = false;
        pulseFreq = -1;
        tellRoom(t("chatover"));
        Util.sleep(1500);

        ArrayList<MultiUser> toKick;
        synchronized (users) {
            toKick = new ArrayList<>(users);
        }
        for (MultiUser u : toKick) {
            u.schedSendDisconnect();
            u.dispose();
            remove(u);
            Util.sleep((int) (Math.random() * joinFreq));
        }
        System.out.println();
    }
    
    public int maxTotal() {
        return maxTotal * proxies.count();
    }
    
    public int maxChatting() {
        return maxChatting * (proxies.count() + 1) * 2 / 3;
    }
    
    public boolean effectiveAutoJoin() {
        return autoJoin && !proxies.isBanned();
    }
    
    public boolean shouldRevive() {
        return !murder && !proxies.isBanned();
    }
    
    protected String showFreqs() {
        return "spam freq: " + joinFreqConstant + "+" + joinFreq + "*n^2, pulse: " + pulseFreq;
    }
    
    public void captchad(final MultiUser source, String challenge) {
        tellAdmin("Captcha ban for " + source.getProxy() + ". " + showFreqs());
        if (proxies.handleCaptcha(source.getProxy())) {
            mainLoop(true);
            return;
        }
        if (proxies.shouldShowBans()) {
            tellRoom(t("captcha"));
        }

    }

    public void banned(ProxyConfig proxy, String type) {
        if (type.startsWith("empty")) {
            tellAdmin("*Possible* ban for " + proxy + ": " + type + ". " + showFreqs());
            proxy.dirty();
            if (proxies.switchProxy())
                mainLoop(true);
            return;
        }
        tellAdmin("Ban for " + proxy + ": " + type + ". " + showFreqs());
        if (proxies.handleBan(proxy)) {
            mainLoop(true);
            return;
        }
        if (proxies.shouldShowBans())
            tellRoom(t("hardban"));
    }

    public void ban(String expr) {
        Ban ban;
        try {
            ban = new Ban(expr);
        } catch (PatternSyntaxException e) {
            tellAdmin("Syntax error: " + e.getMessage());
            return;
        }
        ban(ban);
    }
    public void ban(Ban ban) {
        String canonical = ban.toString();
        synchronized (banLock) {
            Ban[] oldBans = bans;
            for (int i = 0; i < oldBans.length; i++) {
                if (ban.pattern.equals(oldBans[i].pattern)) {
                    if (canonical.equals(oldBans[i].toString()))
                        return;
                    Ban[] newBans = oldBans.clone();
                    newBans[i] = ban;
                    bans = newBans;
                    return;
                }
            }
            Ban[] newBans = Arrays.copyOf(oldBans, oldBans.length + 1);
            newBans[oldBans.length] = ban;
            bans = newBans;
        }
    }
    
    public void unban(String expr) {
        Ban ban;
        try {
            ban = new Ban(expr);
        } catch (PatternSyntaxException e) {
            tellAdmin("Syntax error: " + e.getMessage());
            return;
        }
        unban(ban);
    }
    public void unban(Ban ban) {
        Predicate<Ban> shouldKeep = other -> !ban.pattern.equals(other.pattern);
        synchronized (banLock) {
            if (!Util.all(bans, shouldKeep))
                bans = Util.filter(bans, shouldKeep);
        }
    }
    
    public Ban[] getBans() {
        return bans.clone();
    }

    public void clearBans() {
        synchronized (banLock) {
            bans = new Ban[0];
        }
    }
    
    public void addFlashName(String expr) {
        synchronized (flashes) {
            flashes.add(expr);
        }
    }
    
    public void toggleFlashUser(String name) {
        MultiUser u = userFromName(name);
        if (u == null) {
            System.err.println(name + " isn't a user");
        } else {
            if (u.isFlash()) {
                u.setFlash(false);
                tellAdmin(to -> getDisplayNick(to, u) + " is no longer Flash");
            } else {
                u.setFlash(true);
                tellAdmin(to -> getDisplayNick(to, u) + " is now Flash");
            }
        }
    }

    public void clearFlashes() {
        synchronized (flashes) {
            flashes.clear();
        }
    }
    
    public int spamSize() {
        int size = 0;
        synchronized (users) {
            for (MultiUser u : users)
                if (u.isReady() && !u.isMuted() && !u.isLurker())
                    size++;
        }
        return size;
    }
    
    public int listSize(boolean muted, boolean lurker) {
        int size = 0;
        synchronized (users) {
            for (MultiUser u : users)
                if (u.isReady() && (muted || countMuted || !u.isMuted())
                        && (lurker || !u.isLurker()))
                    size++;
        }
        return size;
    }
    
    public boolean showInList(MultiUser target, MultiUser topic) {
        if (!topic.isReady() || topic.isLurker() && !target.isLurker())
            return false;
        return countMuted || !topic.isMuted() || target.isMuted();
    }

    public long joinFreq() {
        int spamSize = spamSize();
        return joinFreqConstant + (spamSize == 0 ? joinFreq : joinFreq * spamSize * spamSize);
    }
    
    public long minInterval() {
        return (long) Math.pow(joinFreq(), minIntervalExponent);
    }

    public MultiUser userFromName(String name) {
        if (name.matches("[0-9]+"))
            try {
                synchronized (users) {
                    int number = Integer.parseInt(name);
                    if (number >= ids.length)
                        return null;
                    return ids[number];
                }
            } catch (NumberFormatException e) {
                return null;
            }
        else
            synchronized (users) {
                return allUsers.stream()
                    .filter(a -> name.equals(a.getNick()))
                    .findFirst().orElse(allUsers.stream()
                        .filter(a -> name.equalsIgnoreCase(a.getNick()))
                        .findFirst().orElse(null));
            }
    }
    
    protected String getDisplayNick(MultiUser target, MultiUser topic) {
        if (target.showsNumbers())
            return "(" + topic.getNumber() + ") " 
                + (topic.isLurker() ? t("lurkprefix") + topic.getDisplayNick() : topic.getDisplayNick());
        return topic.isLurker() ? t("lurkprefix") + topic.getDisplayNick() : topic.getDisplayNick();
    }
    
    protected String getDisplayTrivia(MultiUser target, MultiUser topic) {
        if (target.showsNumbers())
            return "(" + topic.getNumber() + ") " + (topic.isLurker() ? t("lurkprefix") + topic : topic);
        return topic.isLurker() ? t("lurkprefix") + topic : topic.toString();
    }
    
    public void relay(String template, MultiUser from, String message) {
        relay(template, from, message, null, null);
    }
    
    public void relay(String template_, MultiUser from, String message, String type_, Predicate<MultiUser> extraFilter) {
        if (extraFilter == null)
            from.recordMessage();
        boolean normal = "normal".equals(template_) || "special".equals(template_);
        String template = template_, type = type_;
        Predicate<MultiUser> filter = to -> 
            shouldSendBasic(normal, message, from, to)
            && (extraFilter != null || shouldSendDefault(normal, message, from, to));
        if (extraFilter != null)
            filter = filter.and(extraFilter);
        tellRoom(u -> u.schedSend(t(template, getDisplayNick(u, from), message, type)), filter);
    }
    
    protected boolean shouldSendBasic(boolean normal, String message, MultiUser from, MultiUser to) {
        return !(normal && to == from && from != consoleDummy) && !ignores(to, from);
    }
    
    protected boolean shouldSendDefault(boolean normal, String message, MultiUser from, MultiUser to) {
        return (!from.isMuted() || to.isMuted())
            && (isAdmin(to) || to.isLurker() || !from.isLurker()) 
            && !(!from.isMuted() && to.isMuted() && message.matches("(?i).*mute.*"));
    }
    
    protected boolean isDuplicate(MultiUser user, String text) {
        text = canonicalString(text);
        synchronized (historyLock) {
            if (history.length == 0)
                return false;
            for (Message message : history)
                if (duplicateEquals(message, user, text))
                    return true;
            history[historyPosition].user = user;
            history[historyPosition].text = text;
            historyPosition++;
            historyPosition %= history.length;
            return false;
        }
    }
    
    protected boolean duplicateEquals(Message message, MultiUser user, String text) {
        if (message.user == null || message.text == null)
            return false;
        if (user != message.user && 
                (user.getMsgCount() < 2 || matureHuman >= 0 && user.age() > matureHuman))
            return false;
        return message.text.equals(text);
    }
    
    @Persistent
    private Pattern spaces = Pattern.compile("(?uUs)\\s+");
    @Persistent
    private Pattern invisible = Pattern.compile("(?uUs)[^\\w\\s]+");
    
    protected String canonicalString(String message) {
        message = message.trim();
        message = spaces.matcher(message).replaceAll(" ");
        message = invisible.matcher(message).replaceAll("");
        message = message.toLowerCase(Locale.ROOT);
        return message;
    }
    
    public void setHistorySize(int size) {
        synchronized (historyLock) {
            if (size <= 0) {
                history = new Message[0];
            } else if (size < history.length) {
                int start = Math.max(0, historyPosition - size);
                history = Arrays.copyOfRange(history, start, start + size);
                historyPosition -= start;
                historyPosition %= history.length;
            } else if (size > history.length) {
                historyPosition = history.length;
                history = Arrays.copyOf(history, size);
                for (int i = historyPosition; i < size; i++)
                    history[i] = new Message();
            }
        }
    }
    
    public int getHistorySize() { return history.length; }
    public long getMatureHuman() { return matureHuman; }
    public void setMatureHuman(long matureHuman) { this.matureHuman = matureHuman; }
    
    public void tellLurkers(String message) {
        tellRoom(u -> u.schedTell(message), u -> u.isLurker());
    }

    public void tellMuted(String message) {
        tellRoom(u -> u.schedTell(message), u -> u.isMuted());
    }
    
    public void tellRoom(String message) {
        tellRoom(u -> u.schedTell(message));
    }
    
    public void tellRoom(Consumer<MultiUser> message) {
        tellRoom(message, null);
    }

    public void tellRoom(Consumer<MultiUser> message, Predicate<MultiUser> filter) {
        synchronized (users) {
            if (filter != null)
                allUsers.stream().filter(UserConnection::isReady).filter(filter).forEach(message);
            else
                allUsers.stream().filter(UserConnection::isReady).forEach(message);
        }
    }
    
    public void tellAdmin(String message, boolean verboseOnly, MultiUser... excluded) {
        tellAdmin(message, verboseOnly, excluded.length > 0 
                ? u -> !ignores(u, excluded[0]) && (u != excluded[0] || u == consoleDummy)
                : null);
    }

    public void tellAdmin(String message, boolean verboseOnly, Predicate<MultiUser> filter) {
        synchronized (users) {
            for (MultiUser u : allUsers) {
                if ((filter == null || filter.test(u)) &&
                        u.isReady() && isAdmin(u) && (u.isVerbose() || !verboseOnly)) {
                    u.schedSend(message);
                }
            }
        }
    }
    
    public void tellAdmin(String message, MultiUser... excluded) {
        tellAdmin(message, false, excluded);
    }
    
    public void tellAdmin(Consumer<MultiUser> message) {
        tellRoom(message, this::isAdmin);
    }
    
    public void tellAdmin(Function<MultiUser, String> message) {
        tellRoom(u -> u.schedSend(message.apply(u)), this::isAdmin);
    }

    public boolean isAdmin(MultiUser user) {
        return user == consoleDummy || adminToken == user.getAdminToken();
    }
    
    public boolean isVerboseAdmin(MultiUser user) {
        return user.isVerbose() && isAdmin(user);
    }

    public void deify(MultiUser source, String targetName, boolean on) {
        MultiUser target = userFromName(targetName);
        if (target == null) {
            source.schedTell("No such user");
        } else if (isAdmin(target) == on) {
            source.schedTell("No change made");
        } else {
            target.setAdminToken(on ? adminToken : null);
            tellAdmin(u -> getDisplayTrivia(u, source) + " has " + (on ? "added " : "removed ") 
                    + getDisplayTrivia(u, target) + " as an admin.");
            tellRoom(u -> behave(u).updateFlagsPrivileged(u, target, null));
            if (!on) {
                target.schedTell(getDisplayNick(target, source) + " has removed you as an admin.");
            }
        }
    }
    
    public void voteKick(MultiUser nuisance, MultiUser saviour) {
        Set<MultiUser> voteSet = addMapping(kickVotes, nuisance, saviour);
        try {
            int eligible = 0;
            int votes = 0;
            synchronized (users) {
                for (MultiUser u : users) {
                    if (!u.isReady() || u.isMuted())
                        continue;
                    if (u.idleFor() < lurkingHuman && matureHuman >= 0 && u.age() > matureHuman) {
                        if (u == logBot.get()) {
                            if (voteSet.contains(u)) {
                                eligible++;
                                votes++;
                            }
                        } else {
                            if (u != nuisance)
                                eligible++;
                            if (voteSet.contains(u))
                                votes++;
                        }
                    }
                }
            }
            if (eligible == 0)
                eligible = 1;
            if (votes * 1.0 / eligible > 0.65) {
                kickVoted(nuisance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unvoteKick(MultiUser nuisance, MultiUser saviour) {
        removeMapping(kickVotes, nuisance, saviour);
    }
    
    public void pausePulse(long duration) {
        disablePulseTime = System.currentTimeMillis();
        if (pulseFreq > 0)
            lastPulse = disablePulseTime - pulseFreq;
        pulseCooldownTime = Math.max(0, duration);
        if (pulseCooldownTime > 0)
            tellAdmin("The pulse has been paused for " + Util.minSec(pulseCooldownTime));
    }
    
    public long getPulseCooldown() {
        return Math.max(0, disablePulseTime - System.currentTimeMillis() + pulseCooldownTime);
    }
    
    protected void notifyPulseResumed() {
        if (getPulseCooldown() == 0 && pulseCooldownTime != 0) {
            if (shouldAddPulse())
                tellAdmin("The room has resumed pulsing.");
            else
                tellAdmin("The pulse cooldown has expired, but another setting is preventing pulses from being sent.");
            pulseCooldownTime = 0;
        }
    }
    
    public boolean addPulse() {
        synchronized (users) {
            for (MultiUser other : users)
                if (other.isPulse()) {
                    tellAdmin("Refusing to add more than one pulse! Try kicking the existing one first.");
                    return false;
                }
        }
        proxies.switchIfUnusable();
        boolean accepted;
        synchronized (toothpasteTypes) {
            accepted = !toothpasteTypes.contains(JoinType.PULSE);
        }
        MultiUser uc = makeIdUser().fill(accepted, false, proxies.current());
        uc.pulse();
        synchronized (users) {
            users.add(uc);
            uc.start();
        }
        return true;
    }

    public boolean add(Boolean forceQ, String... params) {
        boolean isQ = forceQ == null ? false : forceQ || Math.random() < (qfreq);
        if (!isQ) {
            synchronized (users) {
                for (MultiUser u : users) {
                    if (!u.isQuestionMode() && !u.isPulse() && !u.isConnected()) {
                        isQ = true;
                        break;
                    }
                }
            }
        }
        boolean accepted;
        synchronized (toothpasteTypes) {
            accepted = isQ ? !toothpasteTypes.contains(JoinType.QUESTION) 
                : !toothpasteTypes.contains(JoinType.TEXT);
        }
        proxies.switchIfUnusable();
        synchronized (users) {
            MultiUser uc = makeIdUser().fill(accepted, isQ, proxies.current());
            if (params != null)
                for (int i = 1; i < params.length; i += 2)
                    if ("lang".equals(params[i - 1]))
                        uc.withLang(params[i]);
                    else if ("topics".equals(params[i - 1]))
                        uc.withTopics(params[i].split(","));
            users.add(uc);
            uc.start();
        }
        return accepted;
    }
    
    protected void resizeIds() {
        int desired = Math.max(users.size() + 1, maxTotal()) * 2 + 1;
        synchronized (users) {
            if (users.size() >= ids.length - 2) {
                ids = Arrays.copyOf(ids, desired);
            } else if (ids.length > Math.max(users.size(), maxTotal()) * 3) {
                for (int i = desired; i < ids.length; i++)
                    if (ids[i] != null || i == lastFreedId)
                        desired = i + 1;
                ids = Arrays.copyOf(ids, desired);
            }
        }
    }
    
    protected int freeId() {
        synchronized (users) {
            for (int i = 0; i < ids.length; i++)
                if (ids[i] == null && i != lastFreedId)
                    return i;
        }
        throw new RuntimeException("No free IDs, can't add user!");
    }

    public boolean add() {
        return add(false);
    }

    protected void remove(MultiUser user) {
        if (user == null) {
            System.err.println("warn: removing null user!");
            return;
        }
        if (user == consoleDummy) {
            tellRoom(u -> behave(u).tellLeft(u, user));
            return;
        }
        boolean wasIn;
        synchronized (users) {
            int number = user.getNumber();
            wasIn = ids.length > number && ids[number] == user;
        }
        user.dispose();
        synchronized (users) {
            lastFreedId = user.getNumber();
            ids[lastFreedId] = null;
            users.remove(user);
        }
        if (wasIn && user.didLive()) {
            tellRoom(u -> behave(u).tellLeft(u, user));
        }
        if (users.isEmpty() && shouldRevive()) {
            Util.sleep(10000);
            autoJoin = true;
        } else if (users.size() == 1 && shouldRevive()) {
            tellRoom(t("onlyleft"));
            Util.sleep(2000);
            add();
            autoJoin = true;
        }
        removeCommon(user);
    }
    
    protected void removeCommon(MultiUser user) {
        user.dispose();
        clearMappings(kickVotes, user);
        clearMappings(ignored, user);
        synchronized (historyLock) {
            for (Message message : history)
                if (message.user == user)
                    message.user = null;
        }
    }
    
    public boolean isTyping() {
        int count = 0;
        synchronized (users) {
            for (MultiUser u : users)
                if (u.isTyping() && u.isAccepted() && !u.isMuted())
                    count++;
        }
        return count != 0;
    }

    protected void typing(MultiUser user) {
        if (user.isAccepted() && !user.isMuted() && !isTyping()) {
            synchronized (users) {
                for (MultiUser u : users) {
                    if (u != user && u.isReady()) {
                        u.schedSendTyping(true);
                    }
                }
            }
        }
    }
    
    protected void stoppedTyping(MultiUser user) {
        if (user.isAccepted() && !user.isMuted() && isTyping()) {
            synchronized (users) {
                for (MultiUser u : users) {
                    if (u != user && u.isReady()) {
                        u.schedSendTyping(false);
                    }
                }
            }
        }
    }
    
    public void hearUntriaged(MultiUser user, String data) {
        if (isEntryLine(data)) {
            user.accept();
            welcome(user);
        } else if (user.isQuestionMode()) {
            return;
        } else if (user.getMsgCount() > 2) {
            kick(user, t("kickspam"));
        } else {
            user.schedTell(t("saytoothpaste", toothpastePrompt));
        }
    }
    
    protected boolean isEntryLine(String line) {
        return !(line.startsWith("|") && line.contains("\""))
            && toothpastePattern.matcher(line).find();
    }
    
    public String getEntryPattern() { return toothpastePattern.toString(); }
    public void setEntryPattern(String entryPattern) { toothpastePattern = Pattern.compile(entryPattern); }
    
    public String getEntryPrompt() { return toothpastePrompt; }
    public void setEntryPrompt(String entryPrompt) { toothpastePrompt = entryPrompt; }
    
    protected Collection<Ban> checkBans(String data) {
        List<Ban> matched = new ArrayList<>();
        for (Ban ban : bans)
            if (ban.check(this, null, data) != Ban.Action.PASS)
                matched.add(ban);
        return matched;
    }
    
    protected Ban.Action runBans(MultiUser user, String data) {
        Ban.Action action = Ban.Action.PASS;
        StringBuilder matches = new StringBuilder();
        String comment = null;
        for (Ban ban : bans) {
            Ban.Action current = ban.check(this, user, data);
            if (current != Ban.Action.PASS) {
                matches.append("\nto a " + current + " for pattern " + Objects.toString(ban.comment, ban.pattern));
                if (current.supersedes(action)) {
                    action = current;
                    if (ban.comment != null)
                        comment = ban.comment;
                }
                if (action.isHighest())
                    break;
            }
        }
        if (matches.length() > 0 && (!user.isMuted() || action != Ban.Action.MUTE && action != Ban.Action.MUTE_ALL))
            tellAdmin(user + " sentenced " + matches.substring(1), false, to -> !ignores(to, user));
        if (action != Ban.Action.PASS) {
            relay("special", user, data, action.toString(), user.isMuted() ? this::isVerboseAdmin : this::isAdmin);
            switch (action) {
            case SOFT:
                user.schedTell(comment == null ? t("softban") : t("softbancomment", comment));
                break;
            case WARN:
                user.schedTell(comment == null ? t("warnban") : t("warnbancomment", comment));
                user.warn();
                break;
            case KICK:
                kickBot(user);
                break;
            case MUTE:
            case MUTE_ALL:
                mute(user);
                break;
            default:
                throw new IllegalStateException("weird ban action");
            }
        }
        return action;
    }
    
    public void hear(MultiUser user, String data) {
        stoppedTyping(user);
        data = data.replaceAll("[\u0000-\u0008\u000e-\u001f\u007f-\u009f]", "");
        if (user.getMsgCount() < 3) {
            if (Util.isTailEat(data)) {
                if (user.isPulseEver())
                    lastPulseTailEat = System.currentTimeMillis();
                relay("special", user, data, "TAIL", this::isVerboseAdmin);
                kick(user, t("taileat"));
                return;
            }
            if (data.contains("dropbox") || data.contains("blasze") || data.contains("omegle69")
                    || data.contains("omegle-fuck.com") || data.contains("my twitter is @TinaSealer")
                    || data.contains("Hello beauty! :)") || data.contains("sexy female here u?")
                    || data.contains("kera.pm") || data.matches("^(1[89]|2[0-5]) [fF]") && data.length() < 20
                    || data.contains("io-chat") || data.contains("ha.do/") || data.contains("jmp2.in/")
                    || data.toLowerCase().contains("svetlana") && data.toLowerCase().contains("interest")) {
                relay("special", user, data, "OPEN", this::isAdmin);
                kickBot(user);
                return;
            }
        }
        Ban.Action banAction = data.matches("/[!.].*") && isAdmin(user) ? Ban.Action.PASS : runBans(user, data);
        if (banAction != Ban.Action.PASS && banAction != Ban.Action.MUTE) {
            suppressFlood(user);
            return;
        }
        if (!user.isAccepted()) {
            if (banAction == Ban.Action.PASS)
                relay("special", user, data, "unco", this::isVerboseAdmin);
            isDuplicate(user, data);
            hearUntriaged(user, data);
            return;
        }
        if (user.isFlash())
            data = Util.unflash(data);
        boolean showVerbose = banAction == Ban.Action.PASS && !data.toLowerCase().startsWith("/password");
        if (isSilentCommand(user, data)) {
            if (showVerbose)
                relay("special", user, data, "cmd", this::isVerboseAdmin);
            command(user, data);
        } else if (suppressFlood(user)) {
            if (showVerbose)
                relay("special", user, data, "flood", this::isVerboseAdmin);
            user.schedTell(t("floodtoofast", Double.toString(floodControlTime / 1000.0)));
        } else if (isDuplicate(user, data)) {
            if (showVerbose)
                relay("special", user, data, "dup", this::isVerboseAdmin);
        } else if (data.startsWith("/")) {
            if (showVerbose)
                relay("special", user, data, "cmd", this::isVerboseAdmin);
            command(user, data);
        } else {
            relay("normal", user, data);
            if (user.isMuted()) {
                if (showVerbose)
                    relay("special", user, data, "mute", to -> isVerboseAdmin(to) && !to.isMuted());
                fakeLogBot(data);
            }
        }
    }
    
    public boolean ignores(MultiUser to, MultiUser from) {
        Set<MultiUser> ignoredUsers = ignored.get(to);
        return ignoredUsers != null && ignoredUsers.contains(from);
    }
    
    public Collection<MultiUser> getIgnored(MultiUser to) {
        Collection<MultiUser> ignoredUsers = ignored.get(to);
        if (ignoredUsers == null || ignoredUsers.size() == 0)
            return null;
        List<MultiUser> ret;
        synchronized (ignored) {
            ret = new ArrayList<>(ignoredUsers);
        }
        Collections.sort(ret, Comparator.comparingLong(a -> -a.age()));
        return ret;
    }
    
    protected void sendToMuted(MultiUser from, String data, boolean me) {
        if (!me && data.startsWith("/me ")) {
            data = data.substring(data.indexOf(" ") + 1);
            me = true;
        }
        synchronized (users) {
            for (MultiUser u : users) {
                if ((me || u != from) && u.isAccepted() && u.isMuted() && !ignores(u, from)) {
                    u.schedSend(t(me ? "action" : "normal", getDisplayNick(u, from), data));
                }
            }
        }
    }
    
    public boolean isSilentCommand(MultiUser user, String data) {
        if (isLogBot(user) && data.startsWith("/LogBot "))
            return true;
        return data.startsWith("/") && behave(user).isSilent(data.substring(1).split(" ")[0]);
    }
    
    public void command(MultiUser user, String data) {
        String[] ca = data.split(" ", 2);
        if (ca[0].equalsIgnoreCase("/challenge")) {
            user.schedTell("Login challenge: " + user.getID() + " " + Util.sha1(user.getID() + challenge));
        } else if (ca.length > 1 && ca[0].equalsIgnoreCase("/password")) {
            if (ca[1].equals(password) || ca[1].equals(Util.sha1(user.getID() + password))) {
                user.setAdminToken(adminToken);
                tellAdmin(to -> getDisplayNick(to, user) + " has logged in as admin");
            } else {
                user.setAdminToken(null);
                System.out.println("Failed login with " + ca[1]);
                tellAdmin(to -> getDisplayNick(to, user) + " has entered a wrong password");
                user.schedTell(t("wrongpassword"));
            }
            tellRoom(u -> behave(u).updateFlagsPrivileged(u, user, null));
        } else if (ca[0].equals("/LogBot") && ca.length > 1) {
            try {
                JsonValue info = JsonParser.parse(ca[1]);
                if (info.getKind() == JsonValue.Kind.OBJECT) {
                    hearLogBot(user, info);
                } else {
                    showLogBotError(user, "Not an object");
                }
            } catch (IllegalArgumentException e) {
                showLogBotError(user, "Not valid JSON");
            }
        } else if (ca[0].matches("/[!.].*")) {
            if (isAdmin(user)) {
                commands.adminCommand(ca.length == 1 ? ca[0].substring(2) : ca[0].substring(2) + " " + ca[1], user);
            } else {
                user.schedTell(t("wrongpassword"));
            }
        } else if (ca[0].equalsIgnoreCase("/v")) {
            user.setVerbose(true);
        } else if (ca[0].equalsIgnoreCase("/t")) {
            user.setVerbose(false);
        } else if (!data.isEmpty()) {
            behave(user).command(data.substring(1), user);
        }
    }
    
    protected UserBehaviour behave(MultiUser user) {
        return user.isBot() ? botBehaviour : userBehaviour;
    }
    
    protected void invite(MultiUser user, String... args) {
        if (user.isMuted()) {
            user.schedTell(t(toothpasteTypes.contains(JoinType.QUESTION) ? "invitedc" : "invitedq", 
                    user.getDisplayNick()));
        } else if (System.currentTimeMillis() - lastInvite < minInterval()) {
            user.schedTell(t("invitetoofast"));
        } else if (users.size() >= maxTotal() || spamSize() >= maxChatting()) {
            user.schedTell(t("invitefull"));
        } else {
            lastInvite = System.currentTimeMillis();
            if (add(false, args)) {
                relay("system", user, t("invitedq", user.getDisplayNick()));
            } else {
                relay("system", user, t("invitedc", user.getDisplayNick()));
            }
        }
    }
    
    protected void pm(MultiUser user, String... p) {
        user.recordMessage();
        MultiUser dest = userFromName(p[0]);
        if (dest == null) {
            user.schedTell(t("pmnouser"));
        } else {
            if (user.isLurker() && !dest.isLurker() && dest != consoleDummy) {
                user.schedTell(t("pmlurker"));
                return;
            }
            if ((!user.isMuted() || dest == consoleDummy || dest.isMuted()) && !ignores(dest, user)) {
                dest.schedSend(t("special", getDisplayNick(dest, user), p[1], t("private")));
                user.schedTell(t("pmsent"));
            } else {
                user.schedTell(t("pmsent"));
                if (isLogBot(dest))
                    fakeLogBot(p[1]);
            }
        }
    }
    
    protected void me(MultiUser user, String message) {
        relay("action", user, message);
        if (user.isMuted())
            fakeLogBot(user.getNick() + " " + message);
    }
    
    protected void nick(MultiUser user, String nick) {
        nick(user, nick, false);
    }
    
    protected void nick(MultiUser user, String nick, boolean silent) {
        nick = nick.replace(" ", "-").replace("\n", "").replace("\r", "");
        if (nick.matches("[0-9]+")) {
            user.schedTell(t("nickdigits"));
        } else {
            try {
                String oldNick = user.getNick();
                if (nick.length() > 100)
                    nick = nick.substring(0, 100);
                user.setNick(nick);
                if (!user.isFlash())
                    synchronized (flashes) {
                        for (String flashName : flashes)
                            if (nick.matches(flashName)) {
                                user.setFlash(true);
                                tellAdmin(to -> getDisplayNick(to, user) + " is now Flash");
                                break;
                            }
                    }
                tellRoom(u -> behave(u).tellNickChanged(u, user, oldNick, silent));
            } catch (IllegalArgumentException e) {
                user.schedTell(t("nicktaken"));
            }
        }
    }
    
    protected void ignore(MultiUser user, String name) {
        MultiUser dest = userFromName(name);
        if (dest == null) {
            user.schedTell(t("pmnouser"));
        } else if (dest == user || dest == consoleDummy) {
            user.schedTell(t("cannotignore"));
        } else if (ignores(user, dest)) {
            user.schedTell(t("alreadyignored", dest.getDisplayNick()));
        } else {
            addMapping(ignored, user, dest);
            behave(user).updateFlags(user, dest, t("addignore", dest.getDisplayNick()));
        }
    }
    
    protected void unignore(MultiUser user, String name) {
        MultiUser dest = userFromName(name);
        if (dest == null) {
            user.schedTell(t("pmnouser"));
        } else if (!ignores(user, dest)) {
            user.schedTell(t("notignored", dest.getDisplayNick()));
        } else {
            removeMapping(ignored, user, dest);
            behave(user).updateFlags(user, dest, t("removeignore", dest.getDisplayNick()));
        }
    }

    protected void patUser(MultiUser source, String name) {
        MultiUser target = userFromName(name);
        if (target == null) {
            source.schedTell(t("pmnouser"));
        } else if (source == target) {
            source.schedTell(t("noselfpat"));
        } else if (System.currentTimeMillis() - source.lastPat < patCooldown) {
            source.schedTell(t("pattoofast"));
        } else {
            target.patCount++;
            source.lastPat = System.currentTimeMillis();
            relay("system", source, t("patuser", source.getDisplayNick(), target.getDisplayNick(), target.patCount));
        }
    }
    
    protected void slashkick(MultiUser user, String name) {
        if (userFromName(name) == null) {
            user.schedTell(t("kicknouser"));
        } else if (matureHuman < 0) {
            user.schedTell(t("commanddisabled"));
        } else if (user.age() < matureHuman) {
            user.schedTell(t("kicktooyoung"));
        } else if (user.isMuted()) {
            tellMuted(t("kickvote", user.getDisplayNick(), userFromName(name).getDisplayNick()));
        } else {
            MultiUser toKick = userFromName(name);
            relay("system", user, t("kickvote", user.getDisplayNick(), toKick.getDisplayNick()));
            voteKick(toKick, user);
        }
    }
    
    protected void slashdontkick(MultiUser user, String name) {
        if (userFromName(name) == null) {
            user.schedTell(t("dontkicknouser"));
        } else if (user.isMuted()) {
            tellMuted(t("dontkickvote", user.getDisplayNick(), userFromName(name).getDisplayNick()));
        } else {
            MultiUser toKick = userFromName(name);
            unvoteKick(toKick, user);
            relay("system", user, t("dontkickvote", user.getDisplayNick(), toKick.getDisplayNick()));
        }
    }
    
    protected void spam(MultiUser user) {
        spam(user, false);
    }
    
    protected void spam(MultiUser user, boolean silent) {
        long joinRestSec = joinFreq() * 3 / 2000;
        long joinFreqSec = joinFreq() / 1000;
        if (autoJoin) {
            if (proxies.isBanned()) {
                user.schedTell(t("spambanned"));
            } else if (spamSize() >= maxChatting()) {
                user.schedTell(t("spampausedfull", maxChatting()));
            } else if (users.size() >= maxTotal()) {
                user.schedTell(t("spampausedlingering", joinRestSec));
            } else {
                user.schedTell(t("spamalreadyon", joinFreqSec));
            }
        } else if (user.isMuted() && !silent) {
            relay("system", user, t("spamon", user.getDisplayNick()));
        } else {
            Consumer<String> tell = silent ? msg -> tellAdmin(msg) : msg -> relay("system", user, msg);
            autoJoin = true;
            tell.accept(t("spamon", user.getDisplayNick()));

            if (proxies.isBanned()) {
                tell.accept(t("spambanned"));
            } else if (spamSize() >= maxChatting()) {
                tell.accept(t("spampausedfull", maxChatting()));
            } else if (users.size() >= maxTotal()) {
                tell.accept(t("spampausedlingering", joinRestSec));
            }
        }
    }
    
    protected void nospam(MultiUser user) {
        nospam(user, false);
    }
    
    protected void nospam(MultiUser user, boolean silent) {
        if (!autoJoin) {
            user.schedTell(t("spamalreadyoff"));
        } else {
            if (silent || !user.isMuted())
                autoJoin = false;
            if (silent)
                tellAdmin(t("spamoff", user.getDisplayNick()));
            else
                relay("system", user, t("spamoff", user.getDisplayNick()));
        }
    }
    
    protected void heyadmin(MultiUser user, String message) {
        String adminMessage = user + " is calling you";
        if (!message.isEmpty())
            adminMessage += ": " + message;
        tellAdmin(adminMessage, false, user);
        relay("system", user, t("admincalled", user.getDisplayNick()));
        if (!user.isMuted() && !ignores(consoleDummy, user))
            System.out.println("\n\n\nSomeone fucking called you!\n\n\n");
    }

    protected void idSelf(MultiUser user) {
        String pulse;
        synchronized (users) {
            pulse = users.stream()
                .filter(MultiUser::isPulse)
                .map(u -> t("pulseid", u.getNick(), u.getPulseWords(), u.getRandid(), Util.minSec(u.idleFor())))
                .findFirst().orElse(t("idnopulse"));
        }
        user.schedTell(t("idreply", user.getNickAndTrivia(), pulse, chatName, MultiUser.MOTHERSHIP));
    }
    
    protected void idOther(MultiUser user, String name) {
        MultiUser dest = userFromName(name);
        if (dest == null)
            user.schedTell(t("pmnouser"));
        else
            user.schedTell(dest.getNickAndTrivia());
    }
    
    protected void showLogBotError(MultiUser user, String message) {
        user.schedTell(message);
        relay("logboterror", user, message, null, this::isAdmin);
    }
    
    protected void hearLogBot(final MultiUser bot, final JsonValue info) {
        if (bot.getMsgCount() < 3 && info.getMap().containsKey("salt")) {
            bot.setLogBotSalt(info.getMap().get("salt").toString());
            bot.setBot(true);
            bot.setLurkPreference(false);
            final String randomString = Util.makeRandid(Util.ALNUM_PIECES, 32);
            final Runnable setUpWithoutAuth = () -> {
                final MultiUser current = getLogBot();
                if (current == null || current == bot) {
                    logBotPrivileged = false;
                    setUpLogBot(bot, info, null);
                } else if (logBotPrivileged) {
                    refuseLogBot(bot, "trying to replace privileged");
                } else {
                    new LogBotRequest(current, "action", "usercommand", "command", "<3",
                            "comment", "someone is trying to replace you") {
                        void callback(String output) {
                            refuseLogBot(bot, "trying to replace unprivileged");
                        }
                        void timeout() {
                            logBotPrivileged = false;
                            setUpLogBot(bot, info, current);
                        }
                    };
                }
            };
            if (logBotAuthEnabled)
                new LogBotRequest(bot, "action", "auth", "random_string", randomString) {
                    String makeId() { return randomString; }
                    void callback(String output) {
                        if (!tryAuth())
                            failAuth();
                    }
                    void timeout() {
                        setUpWithoutAuth.run();
                    }
                    boolean tryAuth() {
                        if (!tryAuth(bot.getProxy())) {
                            ProxyConfig alt = proxies.getLogBotAuthAlt(bot.getProxy());
                            return alt != null && tryAuth(alt);
                        }
                        return true;
                    }
                    boolean tryAuth(ProxyConfig proxy) {
                        try {
                            if (bot.authLogBot(randomString, proxy)) {
                                final MultiUser current = getLogBot();
                                if (logBotPrivileged && current != null)
                                    mute(current);
                                logBotPrivileged = true;
                                setUpLogBot(bot, info, current);
                            } else {
                                refuseLogBot(bot, "bad auth");
                            }
                            return true;
                        } catch (IOException e) {
                            tellAdmin("Couldn't verify logbot_auth over " + proxy.getProxy() + ": " + e);
                            return false;
                        }
                    }
                    void failAuth() {
                        bot.schedSend("Abandoning LogBot auth, setting you up as unprivileged");
                        tellAdmin(to -> "Abandoning LogBot auth, setting " + getDisplayNick(to, bot) + " up as unprivileged");
                        setUpWithoutAuth.run();
                    }
                };
            else
                setUpWithoutAuth.run();
        } else {
            if (bot == logBot.get() && info.getMap().get("usercommands") != null) {
                logBotEnabled = info.getMap().get("usercommands").getBoolean();
            }
            Object id = info.getMap().get("id");
            if (id == null)
                id = info.getMap().get("random_string");
            if (id != null) {
                LogBotRequest req = logBotRequests.get(id.toString());
                if (req != null && bot == req.user.get())
                    req.hear(info);
                else
                    UserConnection.log("Couldn't find request for " + info);
            }
        }
    }
    
    protected void refuseLogBot(MultiUser bot, String reason) {
        bot.schedTell(t("nocommand"));
        tellAdmin(to -> getDisplayNick(to, bot) + " refused as LogBot: " + reason);
    }
    
    protected void setUpLogBot(MultiUser user, JsonValue info, MultiUser old) {
        if (info.getMap().get("usercommands") != null)
            logBotEnabled = info.getMap().get("usercommands").getBoolean();
        logBot = new WeakReference<>(user);
        tellRoom(to -> { if (to != user) behave(to).updateFlags(to, user, null); });
        if (old != null)
            old.schedTell(user + " has replaced you as LogBot");
        if (logBotPrivileged)
            tellAdmin(to -> getDisplayNick(to, user) + " is privileged LogBot");
        else
            tellAdmin(to -> getDisplayNick(to, user) + " is unprivileged LogBot");
        user.schedTell(t("logbotwelcome"));
        behave(user).command("list", user);
    }
    
    protected StringBuilder welcomeNotifyAndCollect(MultiUser user) {
        StringBuilder otherUsers = new StringBuilder();
        tellRoom(u -> {
            behave(u).tellJoined(u, user);
            if (u != consoleDummy && !u.isLurker() && (countMuted || user.isMuted()))
                synchronized (otherUsers) {
                    otherUsers.append(", ").append(u.getNick());
                }
        }, u -> u != user && u.isReady());
        return otherUsers;
    }
    
    protected void welcomeTriaged(MultiUser user, boolean isInformed) {
        boolean separateFirstLine = Math.random() > 0.66;
        String firstLine = randomize(tRandom("youHaveEnteredSingle"), tRandom("youHaveEnteredSingle"), tRandomLine("youHaveEntered")) 
            + tRandomLine("rovingGroupChat");
        if (!isInformed && separateFirstLine) {
            user.schedTell(firstLine);
        }

        if (spamSize() < 1) {
            user.schedTell(tRandomLine("firstUserLine")
                    .replace("__room__", tRandom("room"))
                    + t("emptyRoomUsername", user.getNick()));
            if (shouldRevive()) {
                add();
            }
        } else {
            StringBuilder otherUsers = welcomeNotifyAndCollect(user);
            boolean showUsers = otherUsers.length() > 2 && Math.random() > 2d / 5;
            int userCount = showUsers ? listSize(user.isMuted(), false) : spamSize();
            userCount--;
            String countLine = tRandom("nOthers").replace("__room__", tRandom("room")).replace("__others__",
                    tRandom("others")).replace("__num__", String.valueOf(userCount)) + (showUsers ? ": " : ".");
            if (!separateFirstLine && !isInformed && Math.random() > 0.5) {
                user.schedTell(firstLine + countLine);
            } else {
                user.schedTell(countLine);
            }
            if (showUsers) {
                if (listSize(user.isMuted(), false) > 5 || Math.random() > 0.5) {
                    user.schedTell("    " + otherUsers.substring(2));
                } else {
                    for (String otherUser : otherUsers.substring(2).split(", ")) {
                        user.schedTell("    " + otherUser);
                    }
                }
            }
            user.schedTell(t("normalUsername", user.getNick()));
        }
        user.schedTell(t("shortHelp"));
    }
    
    protected void inform(MultiUser user) {
        Util.sleep((int) (Math.random() * 700));

        String stumbledLine = tRandomLine("stumbledLine");
        String flauntLine = tRandomLine("flauntLine");
        String pasteLine = String.format(tRandom("sayToothpasteToEnter").replace("__room__", tRandom("room")), toothpastePrompt);

        if (Math.random() < 0.5) {
            if (Math.random() < 0.5) {
                user.schedTell(stumbledLine);
                user.schedTell(flauntLine);
                user.schedTell(pasteLine);
            } else {
                user.schedTell(stumbledLine + flauntLine);
                user.schedTell(pasteLine);
            }
        } else {
            if (Math.random() < 0.5) {
                user.schedTell(stumbledLine);
                user.schedTell(flauntLine + pasteLine);
            } else {
                user.schedTell(stumbledLine);
                user.schedTell(pasteLine + flauntLine);
            }
        }

        if (spamSize() < 2) {
            user.schedTell("(" + tRandomLine("emptyLine").replace("__room__", tRandom("room")) + ")");
        }
        user.inform();
    }

    protected void welcome(MultiUser user) {
        if (user.isAccepted()) {
            welcomeTriaged(user, user.isInformed());
        } else if (!user.isInformed()) {
            inform(user);
        }

    }
    
    protected void annPrivate(MultiUser from, String to, String message) {
        MultiUser toUser = userFromName(to);
        if (toUser == null) {
            from.schedTell("No such user");
        } else {
            toUser.schedTell(message);
        }
    }
    
    protected void clearIgnore(MultiUser user) {
        synchronized (ignored) {
            ignored.remove(user);
        }
        user.schedTell(t("ignorecleared"));
    }
    
    protected void clearAllIgnore() {
        synchronized (ignored) {
            ignored.clear();
        }
        tellAdmin("All ignore lists have been cleared!");
    }

    protected void kickVoted(MultiUser u) {
        kick(u, t("kickvoted"));
    }

    protected void kickInactive(MultiUser u) {
        kick(u, t("kickinactive"));
    }

    protected void kickBot(MultiUser u) {
        kick(u, t("kickspam"));
    }

    protected void kickFlood(MultiUser u) {
        kick(u, t("kickflood", floodKickSize, Double.toString(floodKickTime / 1000.0)));
    }

    protected void kick(MultiUser u, String message) {
        if (u == null) {
            System.err.println("not a user (tried to kick: " + message + ")");
        } else {
            u.setKickReason(message);
            u.schedSendDisconnect();
            remove(u);
            if (!u.didLive())
                System.out.println(u.getNick() + " has been kicked: " + message);
        }
    }

    boolean kick(String name, String message) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        kick(u, message);
        return true;
    }

    protected void mute(MultiUser u) {
        if (!u.isMuted()) {
            u.setMuted(true);
            tellRoom(target -> behave(target).updateFlagsPrivileged(target, u, 
                    "Muted: " + getDisplayNick(target, u)));
        }
    }

    boolean mute(String name) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        mute(u);
        return true;
    }

    protected void unmute(MultiUser u) {
        if (u.isMuted()) {
            u.setMuted(false);
            tellRoom(target -> behave(target).updateFlagsPrivileged(target, u, 
                    "Unmuted: " + getDisplayNick(target, u)));
        }
    }

    boolean unmute(String name) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        unmute(u);
        return true;
    }
    
    boolean showJoin(String name) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        tellRoom(target -> userBehaviour.tellJoined(target, u));
        return true;
    }
    
    boolean showLeave(String name, String reason) {
        MultiUser u = userFromName(name);
        if (u == null)
            return false;
        tellRoom(target -> userBehaviour.tellLeft(target, u, reason, true));
        return true;
    }
    
    public StringBuilder appendFlags(MultiUser u, StringBuilder list) {
        if (list == null)
            list = new StringBuilder();
        if (u.isPulse())
            list.append('P');
        else if (!u.isAccepted())
            list.append('U');
        if (!u.isCreated())
            list.append('Z');
        else if (u.getCaptchaSiteKey() != null)
            list.append("C");
        else if (!u.isConnected())
            list.append('W');
        if (u.isMuted())
            list.append('M');
        if (isAdmin(u))
            list.append('A');
        return list;
    }
    
    public StringBuilder appendFlags(MultiUser target, MultiUser topic, StringBuilder list) {
        // why is isPrivileged in UserBehaviour again?
        boolean privileged = behave(target).isPrivileged(target);
        if (privileged)
            list = appendFlags(topic, list);
        else if (list == null)
            list = new StringBuilder();
        
        if (topic == logBot.get())
            list.append('L');
        if (topic.getLurkPreference() == Boolean.FALSE)
            list.append('T');
        else if (topic.isLurker())
            list.append('8');
        if (privileged && ignores(topic, target))
            list.append("D");
        if (ignores(target, topic))
            list.append("I");
        return list;
    }
    
    protected boolean isLogBot(MultiUser u) {
        return u == logBot.get() && !u.isMuted();
    }
    
    protected MultiUser getPrivilegedLogBot() {
        if (!logBotPrivileged) return null;
        return getLogBot();
    }
    
    protected MultiUser getLogBot() {
        MultiUser u = logBot.get();
        if (u != null && !u.isMuted())
            synchronized (users) {
                if (users.contains(u))
                    return u;
            }
        return null;
    }
    
    protected boolean hasLogBot() {
        return getLogBot() != null;
    }
    
    public String getLogBotKey(MultiUser user) {
        String key = Util.sha256(user.getLogBotSalt() + "-" + System.currentTimeMillis() / 10000);
        if (key.length() > 16)
            key = key.substring(0, 16);
        return key;
    }
    
    public Map<String, Object> fillBotMessage(MultiUser bot, Map<String, Object> message) {
        message.put("time", System.currentTimeMillis() / 1000);
        if (!bot.getLogBotSalt().isEmpty())
            message.put("key", getLogBotKey(bot));
        return message;
    }
    
    class LogBotRequest {
        final WeakReference<MultiUser> user;
        final JsonValue args;
        final String id;
        final long time;
        LogBotRequest(MultiUser user, Object... args) {
            this.user = new WeakReference<>(user);
            this.id = makeId();
            Map<String, Object> map = Util.mapHack(args);
            synchronized (logBotRequests) {
                time = System.currentTimeMillis();
                map.put("key", getLogBotKey(user));
                map.put("time", System.currentTimeMillis() / 1000);
                if (getClass() != LogBotRequest.class) {
                    map.put("id", id);
                    logBotRequests.put(id, this);
                }
            }
            this.args = JsonValue.wrap(map);
            user.schedSend("#A " + this.args);
        }
        String makeId() { return Util.makeRandid(); }
        void hear(JsonValue info) {
            synchronized (logBotRequests) {
                logBotRequests.remove(id);
            }
            MultiUser user = this.user.get();
            if (user == null)
                return;
            JsonValue output = info.getMap().get("output");
            if (output != null && output.getKind() != JsonValue.Kind.NULL 
                    && output.toString() != "")
                callback(output.toString());
            else
                callback(null);
        }
        void callback(String output) {}
        void timeout() {}
        public String toString() {
            return args.toString();
        }
    }
    
    protected void wipeLogBotRequests() {
        List<LogBotRequest> removed = null;;
        synchronized (logBotRequests) {
            Collection<LogBotRequest> vals = logBotRequests.values();
            for (Iterator<LogBotRequest> i = vals.iterator(); i.hasNext(); ) {
                LogBotRequest req = i.next();
                if (System.currentTimeMillis() - req.time > logBotTimeout) {
                    if (removed == null)
                        removed = new ArrayList<>();
                    removed.add(req);
                    i.remove();
                    System.out.println("Timed out: " + req);
                } else {
                    MultiUser user = req.user.get();
                    if (user != null && user.isAlive())
                        continue;
                    if (removed == null)
                        removed = new ArrayList<>();
                    removed.add(req);
                    i.remove();
                    System.out.println("User disappeared: " + req);
                }
            }
        }
        if (removed != null)
            for (LogBotRequest req : removed)
                req.timeout();
    }
    
    protected void fakeLogBot(String data) {
        if (!logBotEnabled) return;
        final MultiUser bot = getLogBot();
        if (bot == null || bot.isMuted()) return;
        String[] parts = data.split("#C +");
        if (parts.length < 2) return;
        new LogBotRequest(bot, "action", "usercommand", "command", parts[1]) {
            void callback(String output) {
                if (output == null)
                    return;
                sendToMuted(bot, output, false);
            }
        };
    }
    
    protected boolean logBotUserCommand(String command) {
        final MultiUser bot = getLogBot();
        if (bot == null) return false;;
        new LogBotRequest(bot, "action", "usercommand", "command", command);
        return true;
    }
    
    protected boolean logBotAction(String action) {
        final MultiUser bot = getLogBot();
        if (bot == null) return false;
        new LogBotRequest(bot, "action", action);
        return true;
    }
    
    protected boolean suppressFlood(MultiUser user) {
        if (floodControlSpan == 0 || floodControlTime < 1 || floodControlSpan > 0 && user.getMsgCount() >= floodControlSpan)
            return false;
        if (user.floodControlMeter.allow(1, floodControlTime))
            return false;
        if (user.getMsgCount() >= 1)
            user.setMsgCount(user.getMsgCount() - 1);
        return true;
    }
    
    protected boolean kickIfFlood(MultiUser user, String data) {
        if (isLogBot(user) && data.startsWith("/LogBot ") 
                || user.floodKickMeter.allow(floodKickSize, floodKickTime))
            return false;
        if (matureHuman < 0 || user.age() < matureHuman || user.isWarned()) {
            kickFlood(user);
        } else {
            user.warn();
            relay("system", user, t("floodwarning", floodKickSize, 
                    String.valueOf(floodKickTime / 1000.0), user.getDisplayNick()));
        }
        return true;
    }
    
    public void proxyMessage(String message) {
        tellAdmin(message);
    }

    @Override
    public void callback(MultiUser user, String method, String data) {
        synchronized (users) {
            if (user != null && !users.contains(user) && !user.isDummy())
                return;
        }
        //System.out.println("callback: '" + user + "', '" + method + "', '" + data + "'");
        if (method.equals("captcha")) {
            if (user.isPulseEver())
                lastPulse = 0;
            captchad(user, data);
        } else if (method.equals("ban")) {
            if (user.isPulseEver())
                lastPulse = 0;
            banned(user.getProxy(), data);
        } else if (method.equals("died")) {
            user.getProxy().dirty();
            proxies.switchProxy();
            tellAdmin("Connection died: " + user);
            remove(user);
        } else if (method.equals("disconnected")) {
            remove(user);
        } else if (method.equals("typing")) {
            typing(user);
        } else if (method.equals("stoppedtyping")) {
            stoppedTyping(user);
        } else if (method.equals("message")) {
            if (!kickIfFlood(user, data))
                hear(user, data);
        } else if (method.equals("connected")) {
            welcome(user);
        } else if (method.equals("interests")) {
            if (user.isAccepted())
                tellAdmin("Joined with " + data);
            else
                tellAdmin(to -> getDisplayNick(to, user) + " connected with " + data);
        } else if (method.equals("pulse success")) {
            if (user.isAccepted())
                tellAdmin("Pulse, joined with " + data);
            else
                tellAdmin(to -> getDisplayNick(to, user) + " pulse, connected with " + data);
            addPulse();
        } else if (method.equals("chatname")) {
            chatName = data;
        } else {
            System.err.println("\n\n\nUNKNOWN CALLBACK METHOD in Multi: '" + method + "'\n\n\n");
        }
    }

    protected void beforeRun() { }
    
    protected void setUp() {
        consoleDummy = makeIdUser().dummy();
        if (rcFiles.isEmpty())
            commands.loadRc(null, consoleDummy);
        else
            for (String file : rcFiles)
                commands.loadRc(file, consoleDummy);
        
        for (String file : jsonFiles)
            loadJson(file);
    }
    
    public void run() {
        if (consoleDummy != null)
            return;
        beforeRun();
        setUp();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.err.print("> ");
            try {
                String cmd = in.readLine();
                if (cmd.startsWith("/")) {
                    command(consoleDummy, cmd);
                } else {
                    commands.adminCommand(cmd, consoleDummy);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected boolean shouldSaveUser(MultiUser user) {
        return user.isConnected();
    }
    
    private static final JsonSerializer<Multi> serializer = 
            new JsonSerializer<Multi>(Multi.class) {
        @Override
        void customSave(Multi t, Map<String, Object> map) {
            if (t == null) return;
            t.pulseFreq = -1;
            t.murder = true;
            t.autoJoin = false;
            map.put("historySize", t.getHistorySize());
            Map<String, JsonValue> usersToSave = new LinkedHashMap<>();
            synchronized (t.users) {
                Function<MultiUser, String> getNumber = u -> String.valueOf(u.getNumber());
                MultiUser logBot = t.getLogBot();
                map.put("logBot", logBot == null ? null : getNumber.apply(logBot));
                map.put("idSize", t.ids.length);
                map.put("kickVotes", JsonValue.wrapMappings(t.kickVotes, getNumber));
                map.put("ignored", JsonValue.wrapMappings(t.ignored, getNumber));
                map.put("admins", t.users.stream().filter(t::isAdmin).map(getNumber)
                        .toArray(String[]::new));
                t.users.removeIf(user -> {
                    if (t.shouldSaveUser(user))
                        usersToSave.put(getNumber.apply(user), user.killAndSave());
                    else
                        user.schedSendDisconnect();
                    t.ids[user.getNumber()] = null;
                    t.removeCommon(user);
                    return true;
                });
                map.put("users", usersToSave);
                map.put("consoleDummy", t.consoleDummy.killAndSave());
            }
            map.put("proxies", t.proxies.save());
            map.put("multiUserStatic", MultiUser.saveStatic());
        }
        @Override
        void customLoad(Multi t, Map<String, JsonValue> map) {
            if (t == null) return;
            JsonValue.doIfPresent(MultiUser::loadStatic, map.get("multiUserStatic"));
            JsonValue.doIfPresent(t.proxies::load, map.get("proxies"));
            JsonValue.doIfPresent(size -> t.setHistorySize(size.getInt()), map.get("historySize"));
            JsonValue.doIfPresent(t.consoleDummy::load, map.get("consoleDummy"));
            
            JsonValue idSize = map.get("idSize"),
                users = map.get("users"),
                admins = map.get("admins"),
                ignored = map.get("ignored"),
                kickVotes = map.get("kickVotes"),
                logBot = map.get("logBot");
            synchronized (t.users) {
                if (!JsonValue.isNull(idSize))
                    t.ids = Arrays.copyOf(t.ids, Math.max(t.ids.length, idSize.getInt()));
                if (!JsonValue.isNull(users)) {
                    users.getMap().forEach((k, v) -> {
                        MultiUser user = t.makeUser();
                        user.setProxy(t.proxies.get(
                                v.getMap().get("proxy").getAs(String[].class)));
                        if (user.getProxy() == null)
                            user.setProxy(t.proxies.get("0", 0));
                        user.load(v);
                        t.ids[user.getNumber()] = user;
                        t.users.add(user);
                    });
                }
                if (!JsonValue.isNull(admins))
                    for (JsonValue id : admins.getList())
                        t.ids[id.getInt()].setAdminToken(t.adminToken);
                if (!JsonValue.isNull(ignored))
                    t.ignored.putAll(ignored.unwrapMappings(t::userFromName, 
                            WeakHashMap<MultiUser, Set<MultiUser>>::new, HashSet<MultiUser>::new));
                if (!JsonValue.isNull(kickVotes))
                    t.kickVotes.putAll(kickVotes.unwrapMappings(t::userFromName, 
                            WeakHashMap<MultiUser, Set<MultiUser>>::new, HashSet<MultiUser>::new));
                if (!JsonValue.isNull(logBot))
                    t.logBot = new WeakReference<>(t.ids[logBot.getInt()]);
                t.users.forEach(MultiUser::start);
                if (t.pulseFreq > 0)
                    t.lastPulse = 0;
            }
            System.gc();
        }
    };
    
    protected synchronized JsonValue killAndSave() {
        tellAdmin("Saving room state...");
        JsonValue result = serializer.save(this);
        tellAdmin("Room state saved");
        return result;
    }
    
    public void killAndSave(String file) {
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.println(killAndSave());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    protected synchronized Multi load(JsonValue spec) {
        tellAdmin("Restoring room state...");
        serializer.load(this, spec);
        tellAdmin("Room state restored");
        commands.adminCommand("list", consoleDummy);
        commands.adminCommand("proxies", consoleDummy);
        return this;
    }
    
    protected void loadJson(String file) {
        try {
            String json;
            try (Scanner s = new Scanner(new File(file));) {
                json = s.useDelimiter("\\Z").next();
            }
            load(JsonParser.parse(json));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    protected void parseArgs(String[] args) {
        for (int i = 1; i < args.length; i += 2)
            parseArg(args[i - 1], args[i]);
    }
    
    protected void parseArg(String k, String v) {
        if ("--rc".equals(k))
            rcFiles.add(v);
        else if ("--json".equals(k))
            jsonFiles.add(v);
    }

    private static Multi m;

    public static void main(String[] args) {
        String messageFile = System.getProperty("messageFile");
        if (messageFile != null && messageFile.contains("svetlana"))
            m = new SvetlanaMulti();
        else
            m = new Multi();
        m.parseArgs(args);
        m.run();
    }

}
