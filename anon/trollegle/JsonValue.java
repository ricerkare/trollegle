package anon.trollegle;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Pattern;

public class JsonValue {

    public enum Kind { STRING, NUMBER, BOOLEAN, NULL, OBJECT, ARRAY; };
    
    public static final JsonValue NULL = new JsonValue();
    
    public static boolean isNull(JsonValue val) {
        return val == null || val == NULL || val.getKind() == Kind.NULL;
    }
    
    public static void doIfPresent(Consumer<JsonValue> action, JsonValue arg) {
        if (!isNull(arg))
            action.accept(arg);
    }
    
    public static <T> void doIfPresent(Consumer<? super T> action, JsonValue arg, Class<T> type) {
        if (!isNull(arg))
            action.accept(arg.getAs(type));
    }
    
    protected JsonValue() {}

    public String toString() {
        return "null";
    }
    public String repr() {
        return toString();
    }
    public int hashCode() {
        return 0;
    }
    public boolean equals(Object o) {
        if (!(o instanceof JsonValue))
            return false;
        return ((JsonValue) o).getKind() == Kind.NULL;
    }
    public Kind getKind() {
        return Kind.NULL;
    }
    public int getInt() {
        return 0;
    }
    public long getLong() {
        return 0l;
    }
    public double getDouble() {
        return 0.0;
    }
    public boolean getBoolean() {
        return false;
    }
    public Map<String, JsonValue> getMap() {
        return Collections.emptyMap();
    }
    public List<JsonValue> getList() {
        return Collections.emptyList();
    }
    
    protected <T> T getAsDefault(Class<T> type) {
        return null;
    }
    
    private static <T> T getAsDefaultInner(Class<T> type, Class<?> argType, Object arg) {
        try {
            Constructor<T> ctor = type.getConstructor(argType);
            return ctor.newInstance(arg);
        } catch (ReflectiveOperationException e) {
            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    public <T> T getAs(Class<T> type) {
        if (type == JsonValue.class)
            return (T) this;
        if (getKind() == Kind.NULL)
            return null;
        Class<?> componentType = type.getComponentType();
        if (componentType == int.class)
            return (T) getList().stream().mapToInt(JsonValue::getInt).toArray();
        if (componentType == double.class)
            return (T) getList().stream().mapToDouble(JsonValue::getDouble).toArray();
        if (componentType == long.class)
            return (T) getList().stream().mapToLong(JsonValue::getLong).toArray();
        if (componentType != null && !componentType.isPrimitive())
            return (T) getAsArrayOf(componentType);
        if (type == String.class)
            return (T) toString().intern();
        if (type == Integer.class)
            return (T) Integer.valueOf(getInt());
        if (type == Double.class || type == Number.class)
            return (T) Double.valueOf(getDouble());
        if (type == Long.class)
            return (T) Long.valueOf(getLong());
        if (type == Boolean.class)
            return (T) Boolean.valueOf(getBoolean());
        if (type == Pattern.class)
            return (T) Pattern.compile(toString());
        if (type.isEnum()) {
            for (Enum t : (Enum[]) type.getEnumConstants())
                if (toString().equals(t.name()) || toString().equals(t.toString()))
                    return (T) t;
             throw new IllegalArgumentException(this + " isn't a constant in " + type);
        }
        T t = getAsDefault(type);
        if (t != null)
            return t;
        t = getFromJsonSerializer(type); // Last because it throws
        if (t != null)
            return t;
        throw new IllegalArgumentException("getAs doesn't support type " + type);
    }

    private <T> T getFromJsonSerializer(Class<T> type) {
        try {
            JsonSerializer<T> serializer = JsonSerializer.getFor(type);
            return serializer.load(type.newInstance(), this);
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw (RuntimeException) e;
            throw new RuntimeException(e);
        }
    }
    
    @SuppressWarnings("unchecked")
    public <T> T[] getAsArrayOf(Class<T> type) {
        return getList().stream().map(el -> el.getAs(type))
                .toArray(len -> (T[]) Array.newInstance(type, len));
    }
    
    public <T> Collection<T> getAsCollectionOf(Class<T> type) {
        return Arrays.asList(getAsArrayOf(type));
    }
    
    public <T> Map<String, T> getAsMapTo(Class<T> type) {
        LinkedHashMap<String, T> result = new LinkedHashMap<>();
        getMap().forEach((k, v) -> result.put(k, v.getAs(type)));
        return result;
    }
    
    public <T, U extends Collection<T>> Map<T, U> unwrapMappings(
            Function<String, T> converter,
            Supplier<? extends Map<T, U>> mapSupplier,
            Supplier<? extends U> collectionSupplier) {
        Map<T, U> result = mapSupplier.get();
        getMap().forEach((k, v) -> {
            T newKey = converter.apply(k);
            T newVal = converter.apply(v.toString());
            if (newKey == null || newVal == null)
                return;
            U u = result.get(newKey);
            if (u == null) {
                u = collectionSupplier.get();
                result.put(newKey, u);
            }
            u.add(newVal);
        });
        return result;
    }
    
    public static <T> JsonValue wrapMappings(Map<T, ? extends Collection<T>> map, 
            Function<T, String> converter) {
        LinkedHashMap<String, JsonValue> result = new LinkedHashMap<>();
        map.forEach((k, v) -> result.put(converter.apply(k),
                JsonValue.wrap(v.stream().map(converter).toArray(String[]::new))));
        return new JsonObject(result);
    }
    
    public static JsonValue wrap(Object in) {
        if (in == null)
            return JsonValue.NULL;
        if (in instanceof JsonValue)
            return (JsonValue) in;
        if (in instanceof Map<?, ?>) {
            Map<String, JsonValue> m = new LinkedHashMap<>();
            for (Map.Entry<?, ?> e: ((Map<?, ?>) in).entrySet())
                m.put(e.getKey().toString(), wrap(e.getValue()));
            return new JsonObject(m);
        }
        if (in instanceof Collection<?>) {
            List<JsonValue> l = new ArrayList<>();
            for (Object o : (Collection<?>) in)
                l.add(wrap(o));
            return new JsonArray(l);
        }
        if (in instanceof Object[]) {
            List<JsonValue> l = new ArrayList<>();
            for (Object o : (Object[]) in)
                l.add(wrap(o));
            return new JsonArray(l);
        }
        if (in instanceof Boolean)
            return new JsonBoolean((Boolean) in);
        if (in instanceof String)
            return new JsonString((String) in);
        if (in instanceof Number)
            return new JsonNumber(((Number) in).doubleValue());
        if (in.getClass().getAnnotation(Persistent.class) != null)
            return wrapInner(in.getClass(), in);
        return new JsonString(in.toString());
    }

    // Extracted for """type safety"""
    private static <T> JsonValue wrapInner(Class<T> type, Object in) {
        return JsonSerializer.getFor(type).save(type.cast(in));
    }
    
    public static class JsonString extends JsonValue {
        String string;
        public JsonString(String string) {
            if (string == null)
                throw new NullPointerException();
            this.string = string;
        }
        public Kind getKind() {
            return Kind.STRING;
        }
        public int hashCode() {
            return string.hashCode();
        }
        public boolean equals(Object o) {
            if (!(o instanceof JsonString))
                return false;
            JsonString os = (JsonString) o;
            return string.equals(os.string);
        }
        protected <T> T getAsDefault(Class<T> type) {
            return getAsDefaultInner(type, String.class, string);
        }
        public String toString() {
            return string;
        }
        public String repr() {
            return Util.reprString(string).toString();
        }
        public int getInt() {
            return Integer.parseInt(string);
        }
        public long getLong() {
            return Long.parseLong(string);
        }
        public double getDouble() {
            return Double.parseDouble(string);
        }
        public boolean getBoolean() {
            return string.length() != 0;
        }
    }
    
    public static class JsonNumber extends JsonValue {
        double number;
        public JsonNumber(double number) {
            this.number = number;
        }
        public Kind getKind() {
            return Kind.NUMBER;
        }
        public int hashCode() {
            long v = Double.doubleToLongBits(number);
            return (int) (v ^ (v >>> 32));
        }
        public boolean equals(Object o) {
            if (!(o instanceof JsonNumber))
                return false;
            return number == ((JsonNumber) o).number;
        }
        protected <T> T getAsDefault(Class<T> type) {
            T result = getAsDefaultInner(type, Double.class, number);
            if (result == null)
                result = getAsDefaultInner(type, double.class, number);
            result = getAsDefaultInner(type, Long.class, (long) number);
            if (result == null)
                result = getAsDefaultInner(type, long.class, (long) number);
            result = getAsDefaultInner(type, Integer.class, (int) number);
            if (result == null)
                result = getAsDefaultInner(type, int.class, (int) number);
            return result;
        }
        public String toString() {
            if (number == Math.floor(number))
                return Long.toString((long) number);
            return Double.toString(number);
        }
        public int getInt() {
            return (int) number;
        }
        public long getLong() {
            return (long) number;
        }
        public double getDouble() {
            return number;
        }
        public boolean getBoolean() {
            return number != 0;
        }
    }

    public static class JsonBoolean extends JsonValue {
        boolean value;
        public JsonBoolean(boolean value) {
            this.value = value;
        }
        public Kind getKind() {
            return Kind.BOOLEAN;
        }
        public int hashCode() {
            return value ? 1 : 0;
        }
        public boolean equals(Object o) {
            if (!(o instanceof JsonBoolean))
                return false;
            return value == ((JsonBoolean) o).value;
        }
        protected <T> T getAsDefault(Class<T> type) {
            T result = getAsDefaultInner(type, Boolean.class, value);
            if (result == null)
                result = getAsDefaultInner(type, boolean.class, value);
            return result;
        }
        public String toString() {
            return Boolean.toString(value);
        }
        public int getInt() {
            return value ? 1 : 0;
        }
        public long getLong() {
            return getInt();
        }
        public double getDouble() {
            return getInt();
        }
        public boolean getBoolean() {
            return value;
        }
    }

    public static class JsonObject extends JsonValue {
        Map<String, JsonValue> object;
        List<JsonValue> list;
        public JsonObject(Map<String, JsonValue> object) {
            this.object = Collections.unmodifiableMap(object);
        }
        public Kind getKind() {
            return Kind.OBJECT;
        }
        public int hashCode() {
            return object.hashCode();
        }
        public boolean equals(Object o) {
            if (!(o instanceof JsonObject))
                return false;
            return ((JsonObject) o).object.equals(object);
        }
        public String toString() {
            StringBuilder sb = new StringBuilder("{");
            for (Map.Entry<String, JsonValue> entry : object.entrySet()) {
                if (sb.length() > 1)
                    sb.append(",");
                sb.append(Util.reprString(entry.getKey()));
                sb.append(":");
                sb.append(entry.getValue().repr());
            }
            sb.append("}");
            return sb.toString();
        }
        public boolean getBoolean() {
            return object.size() != 0;
        }
        public Map<String, JsonValue> getMap() {
            return object;
        }
        public List<JsonValue> getList() {
            if (list != null)
                return list;
            list = Collections.unmodifiableList(new ArrayList<JsonValue>(object.values()));
            return list;
        }
    }

    public static class JsonArray extends JsonValue {
        List<JsonValue> array;
        Map<String, JsonValue> map;
        public JsonArray(List<JsonValue> array) {
            this.array = Collections.unmodifiableList(array);
        }
        public Kind getKind() {
            return Kind.ARRAY;
        }
        public int hashCode() {
            return array.hashCode();
        }
        public boolean equals(Object o) {
            if (!(o instanceof JsonArray))
                return false;
            return ((JsonArray) o).array.equals(array);
        }
        public String toString() {
            StringBuilder sb = new StringBuilder("[");
            for (JsonValue v : array) {
                if (sb.length() > 1)
                    sb.append(",");
                sb.append(v.repr());
            }
            sb.append("]");
            return sb.toString();
        }
        public boolean getBoolean() {
            return array.size() != 0;
        }
        public Map<String, JsonValue> getMap() {
            if (map != null)
                return map;
            Map<String, JsonValue> tempMap = new LinkedHashMap<>();
            for (int i = 0; i < array.size(); i++)
                tempMap.put(Integer.toString(i), array.get(i));
            map = Collections.unmodifiableMap(tempMap);
            return map;
        }
        public List<JsonValue> getList() {
            return array;
        }
    }

}
