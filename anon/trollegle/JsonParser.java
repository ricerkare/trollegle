package anon.trollegle;

import anon.trollegle.JsonValue.JsonString;
import anon.trollegle.JsonValue.JsonNumber;
import anon.trollegle.JsonValue.JsonBoolean;
import anon.trollegle.JsonValue.JsonArray;
import anon.trollegle.JsonValue.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public final class JsonParser extends CommonParser {

    private JsonParser(String input) {
		super(input);
    }
    
    public static JsonValue parse(String input) {
        return new JsonParser(input).parseMessage();
    }
    
    private JsonValue parseMessage() {
        JsonValue val = parseValue();
        if (val == null)
            error();
        if (!end())
            error();
        return val;
    }
    
    private JsonValue parseValue() {
        if (end())
            return null;
        int start = pos;
        while (skipSpace());
        JsonValue val = parseObject();
        if (val == null)
            val = parseArray();
        if (val == null)
            val = parseJsonString();
        if (val == null)
            val = parseBoolean();
        if (val == null)
            val = parseNull();
        if (val == null)
            val = parseNumber();
        if (val == null) {
            pos = start;
            return null;
        }
        while (skipSpace());
        return val;
    }
    
    private JsonValue parseObject() {
        if (end())
            return null;
        int start = pos;
        if (!consumeChar('{'))
            return null;
        while (skipSpace());
        if (end()) {
            pos = start;
            return null;
        }
        if (consumeChar('}'))
            return new JsonObject(Collections.<String, JsonValue>emptyMap());
        Map<String, JsonValue> map = new LinkedHashMap<>();
        while (parsePair(map)) {
            if (end()) {
                pos = start;
                return null;
            }
            if (consumeChar('}'))
                return new JsonObject(map);
            if (!consumeChar(',')) {
                pos = start;
                return null;
            }
        }
        pos = start;
        return null;
    }
    
    private boolean parsePair(Map<String, JsonValue> map) {
        if (end())
            return false;
        int start = pos;
        String key = parseString();
        if (key == null)
            return false;
        while (skipSpace());
        if (!consumeChar(':')) {
            pos = start;
            return false;
        }
        JsonValue val = parseValue();
        if (val == null) {
            pos = start;
            return false;
        }
        map.put(key, val);
        return true;
    }
    
    private JsonValue parseArray() {
        if (end())
            return null;
        int start = pos;
        if (!consumeChar('['))
            return null;
        while (skipSpace());
        if (end()) {
            pos = start;
            return null;
        }
        if (consumeChar(']'))
            return new JsonArray(Collections.<JsonValue>emptyList());
        List<JsonValue> list = new ArrayList<>();
        while (parseMember(list)) {
            if (end()) {
                pos = start;
                return null;
            }
            if (consumeChar(']'))
                return new JsonArray(list);
            if (!consumeChar(',')) {
                pos = start;
                return null;
            }
        }
        pos = start;
        return null;
    }
    
    private boolean parseMember(List<JsonValue> list) {
        JsonValue val = parseValue();
        if (val == null)
            return false;
        list.add(val);
        return true;
    }
    
    private JsonString parseJsonString() {
        String string = parseString();
        if (string == null)
            return null;
        return new JsonString(string);
    }
    
    private String parseString() {
        if (end())
            return null;
        int start = pos;
        while (skipSpace());
        if (end() || !consumeChar('"')) {
            pos = start;
            return null;
        }
        StringBuilder buf = new StringBuilder();
        do {
            if (end()) {
                pos = start;
                return null;
            }
            if (consumeChar('"'))
                return buf.toString();
        } while (parseChars(buf) || parseEscape(buf));
        pos = start;
        return null;
    }
    
    private boolean parseChars(StringBuilder buf) {
        if (end())
            return false;
        int start = pos;
        while (!end() && peek() != '\\' && peek() != '"' && peek() > 0x1f)
            consume();
        if (start == pos)
            return false;
        buf.append(input.substring(start, pos));
        return true;
    }
    
    private boolean parseEscape(StringBuilder buf) {
        if (end() || !consumeChar('\\'))
            return false;
        int start = pos;
        if (end()) {
            pos = start;
            return false;
        }
        switch (peek()) {
        case '\\':
        case '/':
        case '"':
            return addAndConsume(buf, peek());
        case 'b':
            return addAndConsume(buf, '\b');
        case 'f':
            return addAndConsume(buf, '\f');
        case 'n':
            return addAndConsume(buf, '\n');
        case 'r':
            return addAndConsume(buf, '\r');
        case 't':
            return addAndConsume(buf, '\t');
        case 'u':
            break;
        default:
            pos = start;
            return false;
        }
        pos += 5;
        if (end()) {
            pos = start;
            return false;
        }
        try {
            buf.append((char) Integer.parseInt(input.substring(pos - 4, pos), 16));
            return true;
        } catch (NumberFormatException e) {
            pos = start;
            return false;
        }
    }
    
    private boolean addAndConsume(StringBuilder buf, char out) {
        buf.append(out);
        consume();
        return true;
    }
    
    private JsonValue parseBoolean() {
        if (end())
            return null;
        if (consumeString("true"))
            return new JsonBoolean(true);
        if (consumeString("false"))
            return new JsonBoolean(false);
        return null;
    }
    
    private JsonValue parseNull() {
        if (end() || !consumeString("null"))
            return null;
        return JsonValue.NULL;
    }
    
    private JsonValue parseNumber() {
        String expr = consumeRegex(numberMatcher);
        if (expr == null)
            return null;
        try {
            return new JsonNumber(Double.parseDouble(expr));
        } catch (NumberFormatException e) {
            return null;
        }
    }
    
    public static void main(String... args) {
        if (args.length == 0) {
            System.err.println("Give me a JSON message as the first arg");
            System.exit(1);
        }
        System.out.println(parse(args[0]));
    }
    
}
