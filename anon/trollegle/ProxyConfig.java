package anon.trollegle;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.regex.Pattern;

public class ProxyConfig implements Comparable<ProxyConfig> {
    
    public static final long MIN_CAPTCHA_GRACE = 1000 * 60 * 15;
    public static final long BAN_LENGTH = 1000 * 60 * 60 * 24;
    public static final long DIRTY_LENGTH = 1000 * 60 * 7;
    public static final int NUM_RANDIDS = 4;
    public static final int PING_INTERVAL = 300000;
    public static final String IP_CHECK_SITE = "http://api.ipify.org/";
    private Proxy proxy;
    private InetSocketAddress torControl;
    private TorSearch torSearch;
    @Persistent
    private long lastCaptcha, lastCaptchaSubmit, lastBan, lastUse, lastDirty, lastPing;
    @Persistent
    private boolean enabled = true;
    @Persistent
    private int priority;
    private String[] randids;
    @Persistent
    String exitAddress = "";
    
    public ProxyConfig(Proxy proxy) {
        this.proxy = proxy;
        randids = new String[(int) (NUM_RANDIDS / 2 + Math.random() * NUM_RANDIDS)];
        for (int i = 0; i < randids.length; i++) {
            randids[i] = Util.makeRandid();
        }
    }
    
    public Proxy getProxy() { return proxy; }
    
    public void captcha() {
        long time = System.currentTimeMillis();
        lastDirty = time;
        if (time - lastCaptchaSubmit < MIN_CAPTCHA_GRACE)
            lastBan = time;
        lastCaptcha = time;
    }
    public void captchaSubmitted() {
        lastCaptcha = 0;
        lastBan = 0;
        lastDirty = 0;
        lastCaptchaSubmit = System.currentTimeMillis();
    }
    public void ban() {
        lastCaptcha = 0;
        lastBan = System.currentTimeMillis();
    }
    public void dirty() {
        lastDirty = System.currentTimeMillis();
    }
    public void unban() {
        lastCaptcha = 0;
        lastCaptchaSubmit = 0;
        lastBan = 0;
        lastDirty = 0;
    }
    public void undirty() {
        lastDirty = 0;
    }
    
    public String getRandid() {
        return randids[(int) (Math.random() * randids.length)];
    }
    
    public String checkExitAddress() {
        String line;
        try {
            URL url = new URL(IP_CHECK_SITE);
            URLConnection conn = url.openConnection(proxy);
            BufferedReader rd
                    = (new BufferedReader(new InputStreamReader(conn.getInputStream())));
            line = rd.readLine();
            rd.close();
            return line;
        } catch (Exception ex) {
            UserConnection.log(this + " exit ip check: " + ex);
            return null;
        }
    }
    
    public String updateAndGetExitAddress() {
        if (exitAddress.isEmpty()) {
            String newAddress = checkExitAddress();
            if (newAddress != null)
                exitAddress = newAddress;
        }
        return exitAddress;
    }
    
    public String getExitAddress() {
        return exitAddress;
    }
    
    public void use() {
        if (!isSearchingTor())
            lastUse = System.currentTimeMillis();
    }
    
    public boolean isEnabled() { return enabled; }
    public void setEnabled(boolean enabled) { this.enabled = enabled; }
    
    public int getPriority() { return priority; }
    public void setPriority(int priority) { this.priority = priority; }
    
    public boolean isTor() { return torControl != null; }
    public InetSocketAddress getTorControl() { return torControl; }
    public void setTorControl(InetSocketAddress torControl) { this.torControl = torControl; }
    
    public synchronized void startTorSearch(Callback callback) {
        if (isSearchingTor())
            return;
        if (proxy == Proxy.NO_PROXY)
            throw new UnsupportedOperationException();
        torSearch = new TorSearch(this, callback);
        exitAddress = "";
        torSearch.start();
    }
    public boolean isSearchingTor() {
        return torSearch != null && torSearch.isAlive();
    }
    
    public boolean isFirstCaptcha() {
        return lastCaptcha > lastUse && lastCaptcha == lastDirty && !isBanned();
    }
    
    public boolean isBanned() {
        return System.currentTimeMillis() - lastBan < BAN_LENGTH;
    }
    
    public boolean isDirty() {
        return System.currentTimeMillis() - lastDirty < DIRTY_LENGTH;
    }
    
    public boolean canUse() {
        return enabled && !isBanned() && (!isSearchingTor() || torSearch.isAccepted());
    }
    
    public int compareTo(ProxyConfig other) {
        if (this.canUse() && !other.canUse())
            return 1;
        if (!this.canUse() && other.canUse())
            return -1;
        if (!this.isDirty() && other.isDirty())
            return 1;
        if (this.isDirty() && !other.isDirty())
            return -1;
        if (other.priority < this.priority)
            return 1;
        if (other.priority > this.priority)
            return -1;
        return (int) Math.signum(other.lastUse - this.lastUse);
    }
    
    public String toString() {
        String thing = proxy.toString();
        if (priority != 0)
            thing += " (priority " + priority + ")";
        if (!enabled)
            thing += " (disabled)";
        if (isBanned())
            thing += " (banned " + Util.minSec(System.currentTimeMillis() - lastBan) + " ago)";
        else if (isFirstCaptcha())
            thing += " (captcha'd " + Util.minSec(System.currentTimeMillis() - lastCaptcha) + " ago)";
        else if (isDirty())
            thing += " (marked dirty " + Util.minSec(System.currentTimeMillis() - lastDirty) + " ago)";
        else if (lastUse != 0)
            thing += " (last used " + Util.minSec(System.currentTimeMillis() - lastUse) + " ago)";
        if (isSearchingTor())
            thing += " (searching)";
        return thing;
    }
    
    public boolean shouldPing() {
        return System.currentTimeMillis() - PING_INTERVAL >= lastPing;
    }
    
    public void pingStatus() {
        if (!shouldPing()) return;
        lastPing = System.currentTimeMillis();
        String randid = randids[(int) (Math.random() * randids.length)];
        int server = UserConnection.chooseFront();
        String description = randid + " on " + this + " front " + server;
        try {
            URL homeUrl = new URL("https://front" + server + ".omegle.com/status?nocache=" + Math.random() + "&randid=" + randid);
            URLConnection conn = homeUrl.openConnection(proxy);
            Util.fillHeaders(conn, "json");
            BufferedReader rd
                    = (new BufferedReader(new InputStreamReader(conn.getInputStream())));
            String line = rd.readLine();
            UserConnection.logVerbose("Status for " + description + ": " + line);
            rd.close();
        } catch (Exception ex) {
            UserConnection.log("Failed to get status for " + description);
            if (UserConnection.verbose) {
                ex.printStackTrace();
            }
            UserConnection.dirtyFront(server);
        }

    }
    
    private final Pattern removeIP = Pattern.compile("/[^/:]+");
    
    public boolean matches(String regex) {
        Pattern pattern = Pattern.compile("\\b" + regex.replaceAll("(?<!\\\\)\\.", "\\.") + "\\b");
        String full = toString();
        String noIP = removeIP.matcher(full).replaceAll("");
        return pattern.matcher(full).find()
            || pattern.matcher(noIP).find()
            || pattern.matcher(full.replace(":", " ")).find()
            || pattern.matcher(noIP.replace(":", " ")).find();
    }
    
    String[] saveAddress() {
        return saveAddress((InetSocketAddress) proxy.address());
    }
    
    static String[] saveAddress(InetSocketAddress address) {
        if (address == null)
            return null;
        return new String[] {address.getHostString(), String.valueOf(address.getPort())};
    }
    
    static InetSocketAddress loadAddress(String[] spec) {
        return new InetSocketAddress(spec[0], Integer.parseInt(spec[1]));
    }
    
    private static final JsonSerializer<ProxyConfig> serializer = 
            new JsonSerializer<ProxyConfig>(ProxyConfig.class) {
        @Override
        void customSave(ProxyConfig t, Map<String, Object> map) {
            if (t == null) return;
            map.put("type", t.proxy.type().name());
            map.put("address", t.saveAddress());
            map.put("torControl", saveAddress(t.torControl));
            map.put("isSearchingTor", t.isSearchingTor());
        }
        @Override
        void customLoad(ProxyConfig t, Map<String, JsonValue> map) {
            if (t == null) return;
            JsonValue type = map.get("type"),
                address = map.get("address"),
                torControl = map.get("torControl"),
                isSearchingTor = map.get("isSearchingTor");
                
            if (!JsonValue.isNull(type) && !type.toString().equals("DIRECT")
                    && !JsonValue.isNull(address))
                t.proxy = new Proxy(Proxy.Type.valueOf(type.toString()),
                        loadAddress(address.getAs(String[].class)));
            if (!JsonValue.isNull(torControl))
                t.torControl = loadAddress(torControl.getAs(String[].class));
            if (!JsonValue.isNull(isSearchingTor) && isSearchingTor.getBoolean())
                t.dirty();
        }
    };
    
    public JsonValue save() {
        return serializer.save(this);
    }
    
    public ProxyConfig load(JsonValue spec) {
        return serializer.load(this, spec);
    }
    
}
