package anon.trollegle;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayDeque;
import java.util.Optional;
import java.util.Queue;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static anon.trollegle.Util.t;

public class CaptchaMulti extends Multi {

    private static final long CAPTCHA_EXPIRY = 1000 * 31 * 4; // rough guess
    private static final long CAPTCHA_MAX_IDLE = 1000 * 30 * 9; // less than inactiveBot
    private int port = 8000;
    @Persistent
    private String siteKey;
    @Persistent
    private Queue<CaptchaResult> results = new ArrayDeque<>();
    @Persistent
    private int lastBroadcastCount;
    
    @Persistent
    static class CaptchaResult {
        @Persistent final long time = System.currentTimeMillis();
        @Persistent final String siteKey, token;
        CaptchaResult(String siteKey, String token) {
            this.siteKey = siteKey;
            this.token = token;
        }
        CaptchaResult() { this.siteKey = this.token = ""; }
        private static final JsonSerializer<CaptchaResult> serializer = 
            new JsonSerializer<>(CaptchaResult.class);
    }
    
    public static class CaptchaUser extends MultiUser {
        @Persistent private boolean wantsCaptchas;
        @Persistent private String siteKey;
        public void setWantsCaptchas(boolean wantsCaptchas) { this.wantsCaptchas = wantsCaptchas; }
        public boolean wantsCaptchas() { return wantsCaptchas; }
        public void setSiteKey(String siteKey) { this.siteKey = siteKey; }
        public String getSiteKey() { return siteKey; }
        
        public CaptchaUser(Callback<? super MultiUser> callback) { super(callback); }
        
        private static final JsonSerializer<CaptchaUser> serializer = new JsonSerializer<>(CaptchaUser.class);
        @Override
        public JsonValue killAndSave() {
            return serializer.save(this, super.killAndSave());
        }
        @Override
        public MultiUser load(JsonValue spec) {
            super.load(spec);
            serializer.load(this, spec);
            return this;
        }
    }
    
    private void handleHttp(HttpExchange ex) throws IOException {
        if (ex.getRequestMethod().equals("POST")) {
            handleCaptchaResult(ex.getRequestBody());
            ex.getResponseHeaders().add("Location", "/");
            ex.sendResponseHeaders(303, -1);
            ex.getResponseBody().close();
        } else {
            ex.getRequestBody().close();
            if (siteKey == null) {
                writeResponse(ex, 200, "No captchas received yet");
                return;
            }
            String response = null;
            try (InputStream in = CaptchaMulti.class.getResource("/data/captcha.html").openStream();
                    Scanner scanner = new Scanner(in);) {
                scanner.useDelimiter("\\A");
                if (scanner.hasNext())
                    response = scanner.next();
            }
            if (response == null) {
                ex.sendResponseHeaders(500, -1);
                ex.getResponseBody().close();
                return;
            }
            writeResponse(ex, 200, String.format(response, siteKey, siteKey));
        }
    }
    
    private void writeResponse(HttpExchange ex, int status, String response) throws IOException {
        ex.sendResponseHeaders(status, 0);
        try (OutputStreamWriter out = new OutputStreamWriter(ex.getResponseBody(), "UTF-8")) {
            out.write(response, 0, response.length());
        }
    }
    
    private void handleCaptchaResult(InputStream in) throws IOException {
        try (Scanner scanner = new Scanner(in)) {
            scanner.useDelimiter("\\A");
            if (scanner.hasNext()) {
                String values = scanner.next();
                String siteKey = URLDecoder.decode(values.replaceAll("(?s).*?sitekey=([^&]*).*", "$1"), "UTF-8");
                String token = URLDecoder.decode(values.replaceAll("(?s).*?response=([^&]*).*", "$1"), "UTF-8");
                handleCaptchaResult(allUsers.iterator().next(), siteKey, token);
            }
        }
    }
    
    protected void handleCaptchaResult(MultiUser from, String siteKey, String token) {
        Optional<MultiUser> user;
        if (siteKey == null && this.siteKey == null)
            user = Optional.empty();
        else
            synchronized (users) {
                user = users.stream().filter(u -> (siteKey == null ? this.siteKey : siteKey).equals(
                        u.getCaptchaSiteKey())).findFirst();
            }
        if (user.isPresent()) {
            tellAdmin(to -> getDisplayNick(to, from) + " has submitted a captcha; using for " + user.get().getProxy());
            user.get().sendCaptcha(token);
            broadcastCount();
        } else {
            synchronized (results) {
                prune();
                results.offer(new CaptchaResult(siteKey, token));
                tellAdmin(to -> getDisplayNick(to, from) + " has submitted a captcha; storing for later use (" + results.size() + " stored)");
            }
        }
    }
    
    private void prune() {
        long now = System.currentTimeMillis();
        synchronized (results) {
            results.removeIf(result -> now - result.time > CAPTCHA_EXPIRY);
        }
    }
    
    @Override
    public void hear(MultiUser user, String data) {
        String[] parts = data.split(" ", 2);
        if (parts[0].equalsIgnoreCase("/cap")) {
            if (parts.length == 1 || parts[1].trim().isEmpty()) {
                if (user instanceof CaptchaUser) {
                    CaptchaUser cu = (CaptchaUser) user;
                    cu.setWantsCaptchas(true);
                    updateUser(cu, countOpenCaptchas(), true);
                    tellAdmin(to -> getDisplayNick(to, cu) + " is now listening for captchas");
                }
            } else {
                try {
                    JsonValue val = JsonParser.parse(parts[1]);
                    handleCaptchaResult(user, val.getMap().get("key").toString(), 
                            val.getMap().get("token").toString());
                    user.schedTell(t("captchawin"));
                } catch (Exception e) {
                    user.schedTell(t("captchalose"));
                    tellAdmin(to -> getDisplayNick(to, user) + " tried to submit a badly-formed captcha");
                }
            }
        } else if (parts[0].equalsIgnoreCase("/nocap")) {
            if (user instanceof CaptchaUser) {
                CaptchaUser cu = (CaptchaUser) user;
                cu.setWantsCaptchas(false);
                cu.setSiteKey(null);
                tellAdmin(to -> getDisplayNick(to, user) + " is no longer listening for captchas");
            }
        } else {
            super.hear(user, data);
        }
    }
    
    @Override
    public void beforeRun() {
        super.beforeRun();
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
            server.createContext("/", this::handleHttp);
            server.setExecutor(null);
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1); // so that saved state isn't restored
        }
    }

    @Override
    public void captchad(final MultiUser source, String challenge) {
        prune();
        if (proxies.handleCaptcha(source.getProxy())) {
            mainLoop(true);
        } else if (proxies.shouldShowBans()) {
            tellRoom(t("captchapublic"));
        }
        if (source.getProxy().isBanned()) {
            tellAdmin("Captcha (likely ban) for " + source.getProxy() + ". " + showFreqs());
            kick(source, "assuming longer captcha ban");
            return;
        }
        
        siteKey = challenge;
        synchronized (results) {
            if (!results.isEmpty()) {
                tellAdmin("Using pre-entered captcha (" + (results.size() - 1) + " left) for " + source.getProxy());
                source.sendCaptcha(results.poll().token);
                return;
            }
        }
        tellAdmin("Captcha for " + source.getProxy() + ". " + showFreqs());
        broadcastCount();
    }
    
    @Override
    protected void remove(MultiUser user) {
        super.remove(user);
        broadcastCount();
    }
    
    private void sendJson(MultiUser user, Object... json) {
        user.schedSend("#A " + JsonValue.wrap(Util.mapHack(json)));
    }
    
    protected void updateUser(CaptchaUser user, int count, boolean force) {
        if (!user.wantsCaptchas()) return;
        if (siteKey != null && !siteKey.equals(user.getSiteKey())) {
            sendJson(user, "captchas", count, "key", siteKey);
        } else if (force || count != lastBroadcastCount) {
            sendJson(user, "captchas", count);
        }
        user.setSiteKey(siteKey);
    }
    
    protected int countOpenCaptchas() {
        synchronized (users) {
            return (int) users.stream().filter(u -> u.getCaptchaSiteKey() != null && u.idleFor() < CAPTCHA_MAX_IDLE).count();
        }
    }
    
    protected void broadcastCount() {
        int newCount = countOpenCaptchas();
        tellRoom(user -> {
            if (user instanceof CaptchaUser)
                updateUser((CaptchaUser) user, newCount, false);
        });
        lastBroadcastCount = newCount;
    }
    
    @Override
    protected MultiUser makeUser() {
        return new CaptchaUser(this);
    }
    
    @Override
    protected synchronized JsonValue killAndSave() {
        return serializer.save(this, super.killAndSave());
    }
    
    @Override
    protected synchronized Multi load(JsonValue spec) {
        super.load(spec);
        serializer.load(this, spec);
        return this;
    }

    @Override
    protected boolean shouldSaveUser(MultiUser user) {
        return user.isCreated();
    }
    
    private static final JsonSerializer<CaptchaMulti> serializer = 
        new JsonSerializer<>(CaptchaMulti.class);
    
    @Override
    protected void parseArg(String k, String v) {
        if ("--captchaport".equals(k))
            port = Integer.parseInt(v);
        else
            super.parseArg(k, v);
    }

    public static void main(String[] args) {
        Multi m = new CaptchaMulti();
        m.parseArgs(args);
        m.run();
    }
    
}
