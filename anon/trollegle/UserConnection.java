package anon.trollegle;

import anon.trollegle.JsonValue.Kind;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.net.Proxy;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.net.URL;
import java.net.URLConnection;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import static anon.trollegle.Util.fillHeaders;
import static anon.trollegle.Util.t;

public class UserConnection extends Thread {
    
    @Persistent
    private String randid = Util.makeRandid();
    @Persistent
    private String id = "", question;
    private Callback<? super UserConnection> callback;
    @Persistent
    private int msgCount;
    @Persistent
    protected int server = chooseFront();
    @Persistent
    private boolean created, connected, lived, questionMode, dummy;
    private boolean done, typing, seesTyping;
    private Poller poller;
    private final Queue<String> tellQueue = new ArrayDeque<>();
    @Persistent
    private long lastActive = System.currentTimeMillis();
    @Persistent
    private long birth = Long.MAX_VALUE;
    private static final ProxyConfig NO_PROXY = new ProxyConfig(Proxy.NO_PROXY);
    private ProxyConfig proxy = NO_PROXY;
    @Persistent
    private boolean moveProxy;
    @Persistent
    private String[] commonLikes;
    @Persistent
    private String commonLikesFlat;
    @Persistent
    private String lastLine;
    private static final String LANG = "en";
    @Persistent
    private String lang = LANG;
    @Persistent
    private String captchaSiteKey;
    
    @Persistent
    private static String[] defaultTopicsArray = {
            "groupchat", "irc", "groupchats", "chatrooms", "math", "maths", "language", "programming", "government"};
    private static String defaultTopics = topicsToString(defaultTopicsArray);
    private String[] topicsArray = defaultTopicsArray;
    private String topics = defaultTopics;

    static boolean verbose;

    public UserConnection(Callback<? super UserConnection> callback) {
        this.callback = callback;
    }
    
    public UserConnection fill(boolean ignored, boolean questionMode, ProxyConfig proxy) {
        return fill(questionMode, proxy);
    }

    public UserConnection fill(boolean questionMode, ProxyConfig proxy) {
        checkLate();
        this.questionMode = questionMode;
        this.proxy = proxy;
        this.randid = proxy.getRandid();
        return this;
    }
    
    public UserConnection withLang(String lang) {
        checkLate();
        this.lang = lang;
        return this;
    }
    
    protected void checkLate() {
        if (created || connected || done || lived)
            throw new RuntimeException("already set in stone");
    }
    
    protected void stopLookingForCommonLikes() {
        if (connected || done || lived)
            return;
        topics = "";
        topicsArray = new String[0];
        sendAction("stoplookingforcommonlikes", null);
    }
    
    protected void callback(String action, String data) {
        callback.callback(this, action, data);
    }
    protected void callback(String action, int data) {
        callback.callback(this, action, data);
    }
    
    public String getDisplayNick() { return id; }
    
    public int getMsgCount() { return msgCount; }
    protected void setMsgCount(int c) { msgCount = c; }
    public String getID() { return id; }
    public String getRandid() { return randid; }
    public long lastActive() { return lastActive; }
    public String getLastLine() { return lastLine; }

    public long idleFor() {
        return System.currentTimeMillis() - lastActive;
    }

    public long age() {
        return System.currentTimeMillis() - birth;
    }
    
    public ProxyConfig getProxy() { return proxy; }
    protected void setProxy(ProxyConfig proxy) { this.proxy = proxy; }
    public String getQuestion() { return question; }
    public String getLang() { return lang; }
    public boolean isCreated() { return created; }
    public boolean isConnected() { return connected; }
    public String getCaptchaSiteKey() { return captchaSiteKey; }
    protected void setMoveProxy(boolean moveProxy) { this.moveProxy = moveProxy; }

    public boolean isReady() {
        return connected;
    }

    public boolean didLive() {
        return lived;
    }

    public boolean isQuestionMode() { return questionMode; }
    public boolean isTyping() { return typing; }
    public boolean isDummy() { return dummy; }
    
    protected String getTopics() {
        return topics;
    }
    
    public UserConnection withTopics(String... topics) {
        checkLate();
        if (topics != null) {
            topicsArray = topics.clone();
            this.topics = topicsToString(topicsArray);
        }
        return this;
    }
    
    static String topicsToString(String[] topics) {
        if (topics.length == 0)
            return "";
        return "&topics=" + JsonValue.wrap(topics);
    }
    
    public static String getDefaultTopics() {
        return defaultTopics;
    }
   
    private static Pattern nonword = Pattern.compile("\\W+");
    private static String canonicalTopic(String topic) {
        return nonword.matcher(topic).replaceAll("");
    }
    
    public static synchronized void setDefaultTopics(String... topics) {
        if (topics == null) return;
        defaultTopicsArray = Util.map(topics, UserConnection::canonicalTopic, String[]::new);
        defaultTopics = topicsToString(topics);
    }
    
    public static synchronized void addTopics(String... topics) {
        topics = Util.map(topics, UserConnection::canonicalTopic, String[]::new);
        topics = Util.filter(topics, t -> !t.isEmpty() && !Util.contains(defaultTopicsArray, t));
        defaultTopicsArray = Util.concat(defaultTopicsArray, topics);
        defaultTopics = topicsToString(defaultTopicsArray);
    }
    
    public static synchronized void removeTopics(String... topics) {
        defaultTopicsArray = Util.filter(defaultTopicsArray, 
                t -> !Util.contains(topics, canonicalTopic(t)));
        defaultTopics = topicsToString(defaultTopicsArray);
    }
    
    public static String getTopicList() {
        if (defaultTopicsArray.length == 0)
            return "The default topic list is empty.";
            
        return "Default topics: " + String.join(", ", defaultTopicsArray);
    }
    
    protected boolean queueBeforeConnect() { return false; }
    
    @Override
    public String toString() {
        return getNickAndTrivia();
    }
    
    public String getNickAndTrivia() {
        return getDisplayNick() + " (" + getTrivia() + ")";
    }
    
    public String getTrivia() {
        return getQuestionOrTopics() + (lang == LANG ? ", " : ", lang=" + lang + ", ") + getRelevantAge();
    }
    
    protected String getRelevantAge() {
        long age = age();
        if (age < 0)
            return t(created ? "waitingage" : "zombieage", Util.minSec(System.currentTimeMillis() - lastActive));
        return t("age", Util.minSec(age));
    }
    
    protected String getQuestionOrTopics() {
        if (questionMode)
            return question == null ? t("questionmode") : question;
        if (commonLikesFlat != null)
            return commonLikesFlat;
        if (topicsArray.length == 0)
            return t("textmode");
        if (topicsArray == defaultTopicsArray)
            return t("defaulttopics");
        if (topicsArray.length > 7)
            return String.join(", ", Arrays.copyOf(topicsArray, 7)) + " [...]";
        return String.join(", ", topicsArray);
    }

    protected UserConnection dummy() {
        dummy = true;
        connected = true;
        lived = true;
        topics = "";
        topicsArray = new String[0];
        birth = System.currentTimeMillis();
        msgCount = 5;
        return this;
    }
    
    protected void startSent() {
        logVerbose("Connection established for " + getDisplayNick());
    }
    
    public ProxyConfig effectiveProxy() {
        return moveProxy ? NO_PROXY : proxy;
    }
    
    public boolean isUsingProxy() {
        return connected && !moveProxy && proxy.getProxy() != Proxy.NO_PROXY;
    }

    public String establishChat() {
        msgCount = 0;
        try {
            Util.retry(2, "Starting chat for " + getDisplayNick(), exception -> {
                String line = null;
                if (!questionMode) {
                    logVerbose("!! Listening on front" + server);
                }
                URL url = new URL("https://front" + server + ".omegle.com/start?caps=recaptcha2&firstevents=1&spid=&randid=" + randid + (questionMode ? "&wantsspy=1" : getTopics()) + "&lang=" + lang);
                logVerbose("Starting chat for " + getDisplayNick());
                proxy.use();
                URLConnection conn = url.openConnection(proxy.getProxy());
                conn.setDoOutput(true);
                fillHeaders(conn, "json post");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write("");
                wr.flush();
                BufferedReader rd
                        = (new BufferedReader(new InputStreamReader(conn.getInputStream())));
                line = rd.readLine();
                //System.out.println("line=\""+line+"\"");
                wr.close();
                rd.close();
                if (line != null) {
                    if (line.trim().equals("")) {
                        banned("empty reply");
                    } else if (line.trim().equals("{}")) {
                        banned("empty object");
                    } else {
                        created = true;
                        lastActive = System.currentTimeMillis();
                        startSent();
                        handleReply(line);
                    }
                    //System.out.println("id: " + id);
                }
            });
        } catch (Exception ex) {
            connected = false;
            callback("died", ex.getMessage());
            dirtyFront(server);
            if (verbose) {
                ex.printStackTrace();
            } else {
                log(ex.toString());
            }
        }
        return id;
    }
    
    protected void banCleanup() {
        // nothing
    }
    
    protected void banned(String type) {
        banCleanup();
        callback("ban", type);
    }
    
    protected String spaceball(String in) {
        return Util.spaceball(in);
    }
    
    protected boolean sendAction(String action, String msg) {
        while (msg != null && msg.length() > 1500) {
            String left = msg.substring(0, msg.length() < 2500 ? 1000 : 1500);
            int seam = left.lastIndexOf("\nban ?off =");
            if (seam == -1)
                seam = left.lastIndexOf("\n");
            if (seam == -1)
                seam = left.lastIndexOf(". ") + 2;
            if (seam == 1)
                seam = left.lastIndexOf(" ") + 1;
            if (seam == 0 || seam < 300) {
                sendActionInner(action, left);
                msg = msg.substring(left.length());
            } else {
                sendActionInner(action, left.substring(0, seam));
                msg = msg.substring(seam);
            }
        }
        return sendActionInner(action, msg);
    }
    
    protected String formatMessage(String message) {
        return "msg=" + Util.urlEncode(spaceball(message));
    }
    
    protected String formatCaptchaResponse(String message) {
        return "response=" + Util.urlEncode(message);
    }
    
    protected boolean sendActionInner(String action, String msg) {
        return sendActionInner(action, msg, this::formatMessage);
    }

    protected boolean sendActionInner(String action, String msg, UnaryOperator<String> formatter) {
        if (dummy) {
            if (action.equals("send")) {
                log(msg);
            } else {
                log("[Dummy-" + action + "]" + msg);
            }
            return true;
        }
        if (msgCount == -1 || id.isEmpty()) {
            return false;
        }
        try {
            Util.retry(2, "Sending " + action + " (" + msg + ") to " + getDisplayNick(), exception -> {
                URL url = new URL("https://front" + server + ".omegle.com/" + Util.urlEncode(action));
                java.net.HttpURLConnection conn = (java.net.HttpURLConnection) url.openConnection(effectiveProxy().getProxy());
                conn.setDoOutput(true);
                fillHeaders(conn, "post");
                OutputStreamWriter wr
                        = new OutputStreamWriter(conn.getOutputStream());
                if (msg != null) {
                    wr.write(formatter.apply(msg) + "&id=" + id);
                } else {
                    wr.write("id=" + id);
                }
                wr.flush();
                wr.close();
                BufferedReader rd;
                try {
                    rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                } catch (java.io.FileNotFoundException e) {
                    rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                    log(getDisplayNick() + " UserConnection 404");
                    dirtyFront(server);
                }
                rd.readLine();
                rd.close();
            });
            return true;
        } catch (Exception ex) {
            log(getDisplayNick() + " had exception: " + ex.getMessage());
            dirtyFront(server);
            ex.printStackTrace();
            return false;
        }
    }
    
    public void schedSend(String message) {
        if (dummy)
            sendAction("send", message);
        else
            synchronized(tellQueue) {
                tellQueue.add(message);
            }
    }

    public static void log(Object message) {
        long time = System.currentTimeMillis();
        
        System.out.printf("%02d:%02d ", time / 60000 / 60 % 24, time / 60000 % 60);
        System.out.println(message);
    }

    public static void logVerbose(Object message) {
        if (verbose) {
            log(message);
        }
    }

    public void schedTell(String message) {
        schedSend(t("system", null, message));
    }

    public void sendDisconnect() {
        sendAction("disconnect", null);
    }
    
    public void schedSendDisconnect() {
        synchronized (tellQueue) {
            tellQueue.add("\u0091disconnect");
        }
    }

    public void sendTyping(boolean typing) {
        if (typing)
            sendAction("typing", null);
        else
            sendAction("stoppedtyping", null);
    }

    public void schedSendTyping(boolean typing) {
        synchronized (tellQueue) {
            if (typing) {
                if (tellQueue.isEmpty() || !tellQueue.peek().startsWith("\u0091"))
                    tellQueue.add("\u0091type");
            } else if (!tellQueue.isEmpty() && tellQueue.peek().equals("\u0091type")) {
                tellQueue.poll();
            } else if (tellQueue.isEmpty() || !tellQueue.peek().startsWith("\u0091")) {
                tellQueue.add("\u0091stop");
            }
        }
    }
    
    public void sendCaptcha(String response) {
        captchaSiteKey = null;
        proxy.captchaSubmitted();
        sendActionInner("recaptcha", response, this::formatCaptchaResponse);
    }
    
    public void dispose() {
        done = true;
    }



    public void run() {
        if (id == null || id.isEmpty())
            id = establishChat();
        if (id != null) {
            poller = new Poller();
            poller.start();
            handleEvents();
        }
    }
    
    protected class Poller extends Thread {
        public void run() {
            while (!done) {
                Util.sleep(75);
                if (!connected && !queueBeforeConnect()) {
                    synchronized (tellQueue) {
                        tellQueue.clear();
                    }
                    continue;
                }
                while (!tellQueue.isEmpty()) {
                    String message = "";
                    synchronized (tellQueue) {
                        message = tellQueue.poll();
                    }
                    if (message.equals("\u0091type")) {
                        if (!seesTyping) {
                            sendTyping(true);
                            seesTyping = true;
                        }
                    } else if (message.equals("\u0091stop")) {
                        if (tellQueue.isEmpty() || tellQueue.peek().startsWith("\u0091")) {
                            sendTyping(false);
                            seesTyping = false;
                        }
                    } else if (message.equals("\u0091disconnect")) {
                        synchronized (tellQueue) {
                            tellQueue.clear();
                            sendDisconnect();
                        }
                    } else {
                        if (!message.startsWith("["))
                            logVerbose("[" + getDisplayNick() + " is being told]" + message);
                        if (!seesTyping)
                            sendTyping(true);
                        Util.sleep(75 + (int) (30 * Math.log(message.length())));
                        sendAction("send", message);
                        Util.sleep(75);
                        seesTyping = false;
                    }
                }
            }
        }
    }
    
    protected void handleEvents() {
        boolean[] rcvd = {true};
        while (rcvd[0] && !done) {
            try {
                Util.retry(6, "Getting events for " + getDisplayNick(), exception -> {
                    rcvd[0] = false;
                    if (exception instanceof UnknownHostException) {
                        Util.sleep(10000);
                    }
                    URL url = new URL("https://front" + server + ".omegle.com/events");
                    URLConnection conn = url.openConnection(effectiveProxy().getProxy());
                    conn.setDoOutput(true);
                    fillHeaders(conn, "post json");
                    OutputStreamWriter wr
                            = new OutputStreamWriter(conn.getOutputStream());
                    wr.write(new StringBuilder("id=").append(id).toString());
                    wr.flush();
                    BufferedReader rd
                            = (new BufferedReader(new InputStreamReader(conn.getInputStream())));
                    String line;
                    while ((line = rd.readLine()) != null) {
                        if (!line.equals("null")) {
                            rcvd[0] = true;
                            handleReply(line);
                        }
                    }
                    wr.close();
                    rd.close();
                    Util.sleep(200);
                });
            } catch (Exception ex) {
                connected = false;
                callback("died", ex.getMessage());
                dirtyFront(server);
                if (verbose) {
                    ex.printStackTrace();
                } else {
                    log(ex.toString());
                }
            }
        }
    }

    protected void handleReply(String line) {
        JsonValue top = JsonParser.parse(line);
        if (top.getKind() == Kind.OBJECT) {
            if (top.getMap().containsKey("clientID")) {
                id = top.getMap().get("clientID").toString();
            }
            if (top.getMap().containsKey("events")) {
                handleEventsReply(top.getMap().get("events").getList());
            }
        } else if (top.getKind() == Kind.ARRAY) {
            handleEventsReply(top.getList());
        } else {
            log(this + ": Can't handle reply: " + line);
        }
    }
    
    protected void handleEventsReply(List<JsonValue> events) {
        for (JsonValue event : events) {
            if (event.getKind() != Kind.ARRAY 
                    || event.getList().size() < 1 
                    || event.getList().get(0).getKind() != Kind.STRING) {
                log(this + ": Badly formed event: " + event);
                continue;
            }
            handleEvent(event.getList().get(0).toString(), 
                    event.getList().subList(1, event.getList().size()));
        }
    }
    
    protected boolean expectStringArg(List<JsonValue> args) {
        return args.size() != 0 && args.get(0).getKind() == Kind.STRING;
    }
    
    protected void connected() {
        if (!connected) {
            connected = true;
            lived = true;
            setBorn();
            lastActive = System.currentTimeMillis();
            proxy.undirty();
            proxy.use();
            callback("connected", null);
        }
    }
    
    protected void setBorn() {
        if (birth == Long.MAX_VALUE)
            birth = System.currentTimeMillis();
    }
    
    protected void gotMessage(String line) {
        lastLine = line;
        callback("message", line);
    }
    
    protected void hasCommonLikes(String commonLikes) {}
    
    protected void handleEvent(String event, List<JsonValue> args) {
        if (event.equals("recaptchaRequired") || event.equals("recaptchaRejected")) {
            banCleanup();
            if (!expectStringArg(args))
                log(this + ": Captcha challenge missing:" + args);
            else
                callback("captcha", captchaSiteKey = args.get(0).toString());
        } else if (event.equals("antinudeBanned")) {
            banned("antinudeBanned");
        } else if (event.equals("typing")) {
            lastActive = System.currentTimeMillis();
            callback("typing", null);
            typing = true;
        } else if (event.equals("stoppedTyping")) {
            lastActive = System.currentTimeMillis();
            callback("stoppedtyping", null);
            typing = false;
        } else if (event.equals("question")) {
            if (!expectStringArg(args))
                log(this + ": Question missing: " + args);
            else
                question = args.get(0).toString();
            connected();
            Util.sleep(200);
        } else if (event.equals("gotMessage")) {
            connected();
            lastActive = System.currentTimeMillis();
            if (!expectStringArg(args)) {
                log(this + ": Message missing: " + args);
            } else {
                gotMessage(args.get(0).toString());
            }
            typing = false;
            msgCount++;
        } else if (event.equals("strangerDisconnected")) {
            connected = false;
            callback("disconnected", null);
            msgCount = -1;
        } else if (event.equals("connected") && !questionMode) {
            connected();
        } else if (event.equals("commonLikes")) {
            if (args.size() == 0 || args.get(0).getKind() != Kind.ARRAY) {
                log(this + ": Common likes missing: " + args);
            } else {
                commonLikes = args.get(0).getList().stream().map(Object::toString).toArray(String[]::new);
                if (commonLikes.length > 0) {
                    commonLikesFlat = String.join(", ", commonLikes);
                    hasCommonLikes(commonLikesFlat);
                }
            }
        }
    }
    
    private static final JsonSerializer<UserConnection> serializer = 
            new JsonSerializer<UserConnection>(UserConnection.class) {
        @Override
        void customSave(UserConnection t, Map<String, Object> map) {
            if (t == null) return;
            map.put("proxy", t.proxy.saveAddress());
        }
        @Override
        void customLoad(UserConnection t, Map<String, JsonValue> map) {
            if (t == null)
                setDefaultTopics(defaultTopicsArray);
        }
    };
    
    public JsonValue killAndSave() {
        if (!dummy)
            done = true;
        return serializer.save(this);
    }
    
    public UserConnection load(JsonValue spec) {
        serializer.load(this, spec);
        if (connected) created = true;
        return this;
    }
    
    public static JsonValue saveStatic() {
        return serializer.save(null);
    }
    
    public static void loadStatic(JsonValue spec) {
        serializer.load(null, spec);
    }
    
    private static class Front {
        final int id;
        long lastDirty;
        Front(int id) { this.id = id + 1; }
        void dirty() { lastDirty = System.currentTimeMillis(); }
        boolean isDirty() {
            return System.currentTimeMillis() - lastDirty < 10 * 60 * 1000;
        }
    }
    
    private static Front[] fronts = new Front[32];
    private static Comparator<Front> frontComparator = Comparator.comparingLong(a -> a.lastDirty);
    static {
        Arrays.setAll(fronts, Front::new);
        fronts[5].lastDirty = Long.MAX_VALUE; // why are you staring at me
        fronts[9].lastDirty = Long.MAX_VALUE;
    }
    
    public static int chooseFront() {
        synchronized (fronts) {
            Front front = Util.randomize(fronts);
            if (front.isDirty()) {
                Arrays.sort(fronts, frontComparator);
                front = fronts[0];
            }
            return front.id;
        }
    }
    
    public static void dirtyFront(int id) {
        synchronized (fronts) {
            Arrays.stream(fronts)
                .filter(a -> a.id == id)
                .findFirst()
                .ifPresent(Front::dirty);
        }
    }
    
}
